'use strict';
angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
//    'ngStorage',
    'ui.router',
    'ncy-angular-breadcrumb',
    'ui.bootstrap',
    'ui.utils',
    'oc.lazyLoad',
    'LocalStorageModule',
    'angular.morris-chart',
    'angular-md5'
]);
var app =
    angular.module('app')
        .config(
        [
            '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function($controllerProvider, $compileProvider, $filterProvider, $provide) {
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
            }
        ]);


app.config(["$breadcrumbProvider", function($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
        template: '<ul class="breadcrumb"><li><i class="fa fa-home"></i><a href="#">Home</a></li><li ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li></ul>'
    });
}]);
angular.module('app')
    .config([
        '$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
            $ocLazyLoadProvider.config({
                debug: true,
                events: true,
                modules: [
                    {
                        name: 'toaster',
                        files: [
                            'lib/modules/angularjs-toaster/toaster.css',
                            'lib/modules/angularjs-toaster/toaster.js'
                        ]
                    },
                    {
                        name: 'ui.select',
                        files: [
                            'lib/modules/angular-ui-select/select.css',
                            'lib/modules/angular-ui-select/select.js'
                        ]
                    },
                    {
                        name: 'ngTagsInput',
                        files: [
                            'lib/modules/ng-tags-input/ng-tags-input.js'
                        ]
                    },
                    {
                        name: 'daterangepicker',
                        serie: true,
                        files: [
                            'lib/modules/angular-daterangepicker/moment.js',
                            'lib/modules/angular-daterangepicker/daterangepicker.js',
                            'lib/modules/angular-daterangepicker/angular-daterangepicker.js'
                        ]
                    },
                    {
                        name: 'vr.directives.slider',
                        files: [
                            'lib/modules/angular-slider/angular-slider.min.js'
                        ]
                    },
                    {
                        name: 'minicolors',
                        files: [
                            'lib/modules/angular-minicolors/jquery.minicolors.js',
                            'lib/modules/angular-minicolors/angular-minicolors.js'
                        ]
                    },
                    {
                        name: 'textAngular',
                        files: [
                            'lib/modules/text-angular/textAngular-sanitize.min.js',
                            'lib/modules/text-angular/textAngular-rangy.min.js',
                            'lib/modules/text-angular/textAngular.min.js'
                        ]
                    },
                    {
                        name: 'ng-nestable',
                        files: [
                            'lib/modules/angular-nestable/jquery.nestable.js',
                            'lib/modules/angular-nestable/angular-nestable.js'
                        ]
                    },
                    {
                        name: 'angularBootstrapNavTree',
                        files: [
                            'lib/modules/angular-bootstrap-nav-tree/abn_tree_directive.js'
                        ]
                    },
                    {
                        name: 'ui.calendar',
                        files: [
                            'lib/jquery/fullcalendar/jquery-ui.custom.min.js',
                            'lib/jquery/fullcalendar/moment.min.js',
                            'lib/jquery/fullcalendar/fullcalendar.js',
                            'lib/modules/angular-ui-calendar/calendar.js'
                        ]
                    },
                    {
                        name: 'ngGrid',
                        files: [
                            'lib/modules/ng-grid/ng-grid.min.js',
                            'lib/modules/ng-grid/ng-grid.css'
                        ]
                    },
                    {
                        name: 'dropzone',
                        files: [
                            'lib/modules/angular-dropzone/dropzone.min.js',
                            'lib/modules/angular-dropzone/angular-dropzone.js'
                        ]
                    }
                ]
            });
        }
    ]);
'use strict';
angular.module('app')
    .run(
    [
        '$rootScope', '$state', '$stateParams',
        function ($rootScope, $state, $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }
    ]
)
    .config(
    [
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider
                .otherwise('/app/dashboard');
            $stateProvider
                .state('app', {
                    abstract: true,
                    url: '/app',
                    templateUrl: 'views/layout.html'
                })
                .state('app.dashboard', {
                    url: '/dashboard',
                    templateUrl: 'views/dashboard.html',
                    ncyBreadcrumb: {
                        label: 'Dashboard',
                        description: ''
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/charts/sparkline/jquery.sparkline.js',
                                        'lib/jquery/charts/easypiechart/jquery.easypiechart.js',
                                        'lib/jquery/charts/flot/jquery.flot.js',
                                        'lib/jquery/charts/flot/jquery.flot.resize.js',
                                        'lib/jquery/charts/flot/jquery.flot.pie.js',
                                        'lib/jquery/charts/flot/jquery.flot.tooltip.js',
                                        'lib/jquery/charts/flot/jquery.flot.orderBars.js',
                                        'lib/jquery/charts/morris/raphael-2.0.2.min.js',
                                        'lib/jquery/charts/morris/morris.js',

                                        'lib/jquery/charts/flot/jquery.flot.orderBars.js',
                                        'lib/jquery/charts/flot/jquery.flot.tooltip.js',
                                        'lib/jquery/charts/flot/jquery.flot.selection.js',
                                        'lib/jquery/charts/flot/jquery.flot.crosshair.js',
                                        'lib/jquery/charts/flot/jquery.flot.stack.js',
                                        'lib/jquery/charts/flot/jquery.flot.time.js',
                                        'lib/jquery/charts/chartjs/chart.js',
                                        'lib/jquery/charts/sparkline/jquery.sparkline.js',

                                        'app/directives/realtimechart.js',


                                    ]
                                });
                            }
                        ]
                    }
                })

                .state('app.databoxes', {
                    url: '/databoxes',
                    templateUrl: 'views/databoxes.html',
                    ncyBreadcrumb: {
                        label: 'Databoxes',
                        description: 'beyond containers'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/directives/realtimechart.js',
                                        'lib/jquery/charts/flot/jquery.flot.js',
                                        'lib/jquery/charts/flot/jquery.flot.orderBars.js',
                                        'lib/jquery/charts/flot/jquery.flot.tooltip.js',
                                        'lib/jquery/charts/flot/jquery.flot.resize.js',
                                        'lib/jquery/charts/flot/jquery.flot.selection.js',
                                        'lib/jquery/charts/flot/jquery.flot.crosshair.js',
                                        'lib/jquery/charts/flot/jquery.flot.stack.js',
                                        'lib/jquery/charts/flot/jquery.flot.time.js',
                                        'lib/jquery/charts/flot/jquery.flot.pie.js',
                                        'lib/jquery/charts/morris/raphael-2.0.2.min.js',
                                        'lib/jquery/charts/chartjs/chart.js',
                                        'lib/jquery/charts/morris/morris.js',
                                        'lib/jquery/charts/sparkline/jquery.sparkline.js',
                                        'lib/jquery/charts/easypiechart/jquery.easypiechart.js',
                                        'app/controllers/databoxes.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.widgets', {
                    url: '/widgets',
                    templateUrl: 'views/widgets.html',
                    ncyBreadcrumb: {
                        label: 'Widgets',
                        description: 'flexible containers'
                    }
                })
                .state('app.elements', {
                    url: '/elements',
                    templateUrl: 'views/elements.html',
                    ncyBreadcrumb: {
                        label: 'UI Elements',
                        description: 'Basics'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/controllers/pagination.js',
                                        'app/controllers/progressbar.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.fontawesome', {
                    url: '/fontawesome',
                    templateUrl: 'views/font-awesome.html',
                    ncyBreadcrumb: {
                        label: 'FontAwesome',
                        description: 'Iconic Fonts'
                    }
                })
                .state('app.glyphicons', {
                    url: '/glyphicons',
                    templateUrl: 'views/glyph-icons.html',
                    ncyBreadcrumb: {
                        label: 'GlyphIcons',
                        description: 'Sharp and clean symbols'
                    }
                })
                .state('app.typicons', {
                    url: '/typicons',
                    templateUrl: 'views/typicons.html',
                    ncyBreadcrumb: {
                        label: 'Typicons',
                        description: 'Vector icons'
                    }
                })
                .state('app.weathericons', {
                    url: '/weathericons',
                    templateUrl: 'views/weather-icons.html',
                    ncyBreadcrumb: {
                        label: 'Weather Icons',
                        description: 'Beautiful forcasting icons'
                    }
                })
                .state('app.tabs', {
                    url: '/tabs',
                    templateUrl: 'views/tabs.html',
                    ncyBreadcrumb: {
                        label: 'Tabs',
                        description: 'Tabs and Accordions'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/controllers/accordion.js',
                                        'app/controllers/tab.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.alerts', {
                    url: '/alerts',
                    templateUrl: 'views/alerts.html',
                    ncyBreadcrumb: {
                        label: 'Alerts',
                        description: 'Tooltips and Notifications'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('toaster').then(
                                    function () {
                                        return $ocLazyLoad.load({
                                                serie: true,
                                                files: [
                                                    'app/controllers/alert.js',
                                                    'app/controllers/toaster.js'
                                                ]
                                            }
                                        );
                                    }
                                );
                            }
                        ]
                    }
                })
                .state('app.modals', {
                    url: '/modals',
                    templateUrl: 'views/modals.html',
                    ncyBreadcrumb: {
                        label: 'Modals',
                        description: 'Modals and Wells'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/controllers/modal.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.buttons', {
                    url: '/buttons',
                    templateUrl: 'views/buttons.html',
                    ncyBreadcrumb: {
                        label: 'Buttons'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/controllers/button.js',
                                        'app/controllers/dropdown.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.nestablelist', {
                    url: '/nestablelist',
                    templateUrl: 'views/nestable-list.html',
                    ncyBreadcrumb: {
                        label: 'Nestable Lists',
                        description: 'Dragable list items'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load(['ng-nestable']).then(
                                    function () {
                                        return $ocLazyLoad.load(
                                            {
                                                serie: true,
                                                files: [
                                                    'app/controllers/nestable.js'
                                                ]
                                            });
                                    }
                                );
                            }
                        ]
                    }
                })
                .state('app.treeview', {
                    url: '/treeview',
                    templateUrl: 'views/treeview.html',
                    ncyBreadcrumb: {
                        label: 'Treeview',
                        description: "Fuel UX's tree"
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load(['angularBootstrapNavTree']).then(
                                    function () {
                                        return $ocLazyLoad.load(
                                            {
                                                serie: true,
                                                files: [
                                                    'app/controllers/treeview.js'
                                                ]
                                            });
                                    }
                                );
                            }
                        ]
                    }
                })
                .state('app.simpletables', {
                    url: '/simpletables',
                    templateUrl: 'views/tables-simple.html',
                    ncyBreadcrumb: {
                        label: 'Tables',
                        description: 'simple and responsive tables'
                    }
                })
                .state('app.datatables', {
                    url: '/datatables',
                    templateUrl: 'views/tables-data.html',
                    ncyBreadcrumb: {
                        label: 'Datatables',
                        description: 'jquery plugin for data management'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load(['ngGrid']).then(
                                    function () {
                                        return $ocLazyLoad.load(
                                            {
                                                serie: true,
                                                files: [
                                                    'app/controllers/nggrid.js',
                                                    'lib/jquery/datatable/dataTables.bootstrap.css',
                                                    'lib/jquery/datatable/jquery.dataTables.min.js',
                                                    'lib/jquery/datatable/ZeroClipboard.js',
                                                    'lib/jquery/datatable/dataTables.tableTools.min.js',
                                                    'lib/jquery/datatable/dataTables.bootstrap.min.js',
                                                    'app/controllers/datatable.js'
                                                ]
                                            });
                                    }
                                );

                            }
                        ]
                    }

                })
                .state('app.formlayout', {
                    url: '/formlayout',
                    templateUrl: 'views/form-layout.html',
                    ncyBreadcrumb: {
                        label: 'Form Layouts'
                    }
                })
                .state('app.forminputs', {
                    url: '/forminputs',
                    templateUrl: 'views/form-inputs.html',
                    ncyBreadcrumb: {
                        label: 'Form Inputs'
                    }
                })
                .state('app.formpickers', {
                    url: '/formpickers',
                    templateUrl: 'views/form-pickers.html',
                    ncyBreadcrumb: {
                        label: 'Form Pickers',
                        description: 'Data Pickers '
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load(['ui.select', 'ngTagsInput', 'daterangepicker', 'vr.directives.slider', 'minicolors', 'dropzone']).then(
                                    function () {
                                        return $ocLazyLoad.load(
                                            {
                                                serie: true,
                                                files: [
                                                    'app/controllers/select2.js',
                                                    'app/controllers/tagsinput.js',
                                                    'app/controllers/datepicker.js',
                                                    'app/controllers/timepicker.js',
                                                    'app/controllers/daterangepicker.js',
                                                    'lib/jquery/fuelux/spinbox/fuelux.spinbox.js',
                                                    'lib/jquery/knob/jquery.knob.js',
                                                    'lib/jquery/textarea/jquery.autosize.js',
                                                    'app/controllers/slider.js',
                                                    'app/controllers/minicolors.js'
                                                ]
                                            });
                                    }
                                );
                            }
                        ]
                    }
                })
                .state('app.formwizard', {
                    url: '/formwizard',
                    templateUrl: 'views/form-wizard.html',
                    ncyBreadcrumb: {
                        label: 'Form Wizard'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/fuelux/wizard/wizard-custom.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.formvalidation', {
                    url: '/formvalidation',
                    templateUrl: 'views/form-validation.html',
                    ncyBreadcrumb: {
                        label: 'Form Validation',
                        description: 'Bootstrap Validator'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/controllers/validation.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.formeditors', {
                    url: '/formeditors',
                    templateUrl: 'views/form-editors.html',
                    ncyBreadcrumb: {
                        label: 'Form Editors'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load(['textAngular']).then(
                                    function () {
                                        return $ocLazyLoad.load(
                                            {
                                                serie: true,
                                                files: [
                                                    'app/controllers/textangular.js'
                                                ]
                                            });
                                    }
                                );
                            }
                        ]
                    }
                })
                .state('app.forminputmask', {
                    url: '/forminputmask',
                    templateUrl: 'views/form-inputmask.html',
                    ncyBreadcrumb: {
                        label: 'Form Input Mask'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/inputmask/jasny-bootstrap.min.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.flot', {
                    url: '/flot',
                    templateUrl: 'views/flot.html',
                    ncyBreadcrumb: {
                        label: 'Flot Charts',
                        description: 'attractive plotting'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/directives/realtimechart.js',
                                        'lib/jquery/charts/flot/jquery.flot.js',
                                        'lib/jquery/charts/flot/jquery.flot.orderBars.js',
                                        'lib/jquery/charts/flot/jquery.flot.tooltip.js',
                                        'lib/jquery/charts/flot/jquery.flot.resize.js',
                                        'lib/jquery/charts/flot/jquery.flot.selection.js',
                                        'lib/jquery/charts/flot/jquery.flot.crosshair.js',
                                        'lib/jquery/charts/flot/jquery.flot.stack.js',
                                        'lib/jquery/charts/flot/jquery.flot.time.js',
                                        'lib/jquery/charts/flot/jquery.flot.pie.js',
                                        'app/controllers/flot.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.morris', {
                    url: '/morris',
                    templateUrl: 'views/morris.html',
                    ncyBreadcrumb: {
                        label: 'Morris Charts',
                        description: 'simple & flat charts'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/charts/morris/raphael-2.0.2.min.js',
                                        'lib/jquery/charts/morris/morris.js',
                                        'app/controllers/morris.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.sparkline', {
                    url: '/sparkline',
                    templateUrl: 'views/sparkline.html',
                    ncyBreadcrumb: {
                        label: 'Sparkline',
                        description: 'inline charts'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/charts/sparkline/jquery.sparkline.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.easypiechart', {
                    url: '/easypiechart',
                    templateUrl: 'views/easypiechart.html',
                    ncyBreadcrumb: {
                        label: 'Easy Pie Charts',
                        description: 'lightweight charts'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/charts/easypiechart/jquery.easypiechart.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.chartjs', {
                    url: '/chartjs',
                    templateUrl: 'views/chartjs.html',
                    ncyBreadcrumb: {
                        label: 'ChartJS',
                        description: 'Cool HTML5 Charts'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/charts/chartjs/chart.js',
                                        'app/controllers/chartjs.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.profile', {
                    url: '/profile',
                    templateUrl: 'views/profile.html',
                    ncyBreadcrumb: {
                        label: 'User Profile'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/charts/flot/jquery.flot.js',
                                        'lib/jquery/charts/flot/jquery.flot.resize.js',
                                        'app/controllers/profile.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                .state('app.inbox', {
                    url: '/inbox',
                    templateUrl: 'views/inbox.html',
                    ncyBreadcrumb: {
                        label: 'Beyond Mail'
                    }
                })
                .state('app.messageview', {
                    url: '/viewmessage',
                    templateUrl: 'views/message-view.html',
                    ncyBreadcrumb: {
                        label: 'Veiw Message'
                    }
                })
                .state('app.messagecompose', {
                    url: '/composemessage',
                    templateUrl: 'views/message-compose.html',
                    ncyBreadcrumb: {
                        label: 'Compose Message'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load(['textAngular']).then(
                                    function () {
                                        return $ocLazyLoad.load(
                                            {
                                                serie: true,
                                                files: [
                                                    'app/controllers/textangular.js'
                                                ]
                                            });
                                    }
                                );
                            }
                        ]
                    }
                })
                .state('app.calendar', {
                    url: '/calendar',
                    templateUrl: 'views/calendar.html',
                    ncyBreadcrumb: {
                        label: 'Full Calendar'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load(['ui.calendar']).then(
                                    function () {
                                        return $ocLazyLoad.load(
                                            {
                                                serie: true,
                                                files: [
                                                    'app/controllers/fullcalendar.js'
                                                ]
                                            });
                                    }
                                );
                            }
                        ]
                    }
                })

                .state('app.pricing', {
                    url: '/pricing',
                    templateUrl: 'views/pricing.html',
                    ncyBreadcrumb: {
                        label: 'Pricing Tables'
                    }
                })
                .state('app.invoice', {
                    url: '/invoice',
                    templateUrl: 'views/invoice.html',
                    ncyBreadcrumb: {
                        label: 'Invoice Page'
                    }
                })

                .state('register', {
                    url: '/register',
                    templateUrl: 'views/register.html',
                    ncyBreadcrumb: {
                        label: 'Register'
                    }
                })
                .state('lock', {
                    url: '/lock',
                    templateUrl: 'views/lock.html',
                    ncyBreadcrumb: {
                        label: 'Lock Screen'
                    }
                })
                .state('app.typography', {
                    url: '/typography',
                    templateUrl: 'views/typography.html',
                    ncyBreadcrumb: {
                        label: 'Typography'
                    }
                })
                .state('error404', {
                    url: '/error404',
                    templateUrl: 'views/error-404.html',
                    ncyBreadcrumb: {
                        label: 'Error 404 - The page not found'
                    }
                })
                .state('error500', {
                    url: '/error500',
                    templateUrl: 'views/error-500.html',
                    ncyBreadcrumb: {
                        label: 'Error 500 - something went wrong'
                    }
                })
                .state('app.blank', {
                    url: '/blank',
                    templateUrl: 'views/blank.html',
                    ncyBreadcrumb: {
                        label: 'Blank Page'
                    }
                })
                .state('app.grid', {
                    url: '/grid',
                    templateUrl: 'views/grid.html',
                    ncyBreadcrumb: {
                        label: 'Bootstrap Grid'
                    }
                })
                .state('app.versions', {
                    url: '/versions',
                    templateUrl: 'views/versions.html',
                    ncyBreadcrumb: {
                        label: 'BGYL'
                    }
                })
                .state('app.mvc', {
                    url: '/mvc',
                    templateUrl: 'views/mvc.html',
                    ncyBreadcrumb: {
                        label: 'BeyondAdmin Asp.Net MVC Version'
                    }
                })
                .state('app.game', {
                    url: '/game',
                    templateUrl: 'views/game.html',
                    ncyBreadcrumb: {
                        label: '投注页面'
                    }
                })
                .state('app.play', {
                    url: '/play',
                    templateUrl: 'views/play.html',
                    ncyBreadcrumb: {
                        label: '投注页面'
                    }
                })
                ///////////////////////////////////////////
                .state('app.newDeposit', {
                    url: '/newDeposit',
                    templateUrl: 'views/newDeposit.html',
                    ncyBreadcrumb: {
                        label: '充值提案'
                    }
                })
                .state('app.deposit', {
                    url: '/deposit',
                    templateUrl: 'views/deposit.html',
                    ncyBreadcrumb: {
                        label: '充值记录'
                    }
                })
                .state('app.withdraw', {
                    url: '/withdraw',
                    templateUrl: 'views/withdraw.html',
                    ncyBreadcrumb: {
                        label: '提现申请'
                    }
                })
                //////////////////////////////////////
                .state('app.user', {
                    url: '/user',
                    templateUrl: 'views/user.html',
                    ncyBreadcrumb: {
                        label: '会员列表'
                    }
                })
                .state('app.bank', {
                    url: '/bank/:userName?',
                    templateUrl: 'views/userBank.html',
                    ncyBreadcrumb: {
                        label: '会员银行卡'
                    }
                })

                .state('app.illegal', {
                    url: '/illegal',
                    templateUrl: 'views/illegal.html',
                    ncyBreadcrumb: {
                        label: '非法用户'
                    }
                })
                .state('app.loginLog', {
                    url: '/loginLog/:ip?',
                    templateUrl: 'views/LoginLog.html',
                    ncyBreadcrumb: {
                        label: 'IP分析'
                    }
                })
                ////////////////////////////////////////////
                .state('userPage', {
                    url: '/userPage/{userName}',//这里是子类
                    templateUrl: 'views/userDetail.html',
                    ncyBreadcrumb: {
                        label: '会员详情'
                    }
                })
                .state('loginLog', {
                    url: '/loginLog/{userName}',
                    templateUrl: 'views/LoginLog.html',
                    ncyBreadcrumb: {
                        label: '个人IP分析'
                    }
                })
                .state('bank', {
                    url: '/bank/:userName',
                    templateUrl: 'views/userBank.html',
                    ncyBreadcrumb: {
                        label: '个人银行卡'
                    }
                })
                .state('user999', {
                    url: '/user999/:userName',
                    templateUrl: 'views/user999.html',
                    ncyBreadcrumb: {
                        label: '用户账变'
                    }
                })
                .state('app.orderList', {
                    url: '/orderList',
                    templateUrl: 'views/orderList.html',
                    ncyBreadcrumb: {
                        label: '订单列表'
                    }
                })
                .state('orderList', {
                    url: '/order/:userName',
                    templateUrl: 'views/orderList.html',
                    ncyBreadcrumb: {
                        label: '用户订单'
                    }
                })
                .state('orderDetail', {
                    url: '/orderDetail/:orderId',
                    templateUrl: 'views/orderDetail.html',
                    ncyBreadcrumb: {
                        label: '用户订单'
                    }
                })
                ////////////////////////////////////////
                .state('login', {
                    url: '/login',
                    templateUrl: 'views/login.html',
                    ncyBreadcrumb: {
                        label: '登陆'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/controllers/login.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
            ;
        }
    ]
);
'use strict';

angular.module('app')
    .controller('AppCtrl', [
        '$rootScope', '$scope', '$interval', 'localStorageService', '$state', '$timeout', '$filter', '$location', 'UserService',
        function
            ($rootScope, $scope, $interval, localStorageService, $state, $timeout, $filter, $location, UserService) {
            $interval(function () {
                $rootScope.getMsg();
            }, 50000, 0);
            /*  $rootScope.reRequest = function(page){
             if(page!=null){
             $scope.dRequest.page = 1;
             $scope.requestFunc();
             }else{
             $scope.requestFunc();
             }
             };*/
            $rootScope.search = function (uname, bMain) {
                UserService.getDetail({username: uname}).then(
                    function (data) {
                        var regExp = new RegExp("/[^/^&]+$");//这里用正则
                        //var regExp2 = new RegExp("[^/]*&[^/^&]*$");//这里用正则匹配a&b
                        var newPath = $location.url();
                        if (bMain) {
                            newPath =$location.absUrl();
                            var regExp1 = new RegExp("#/S+");
                            newPath = newPath.replace(regExp1, '#/userPage/'+uname);
                            window.open(
                                newPath
                            );
                        } else {
                            if (newPath.match("/$")) {
                                newPath = newPath +uname;
                            } else {
                                newPath = newPath.replace(regExp, "/"+uname);
                            }
                            console.log(newPath);
                            $location.path(newPath);
                        }
                    },
                    function (data) {
                        //alert(data.msg);
                    }
                );
                //$location.path($location.url()+$rootScope.userName);


            };
            $rootScope.setStartTime = function (sTime) {
                $rootScope.dRequest.startTime = $filter('date')(sTime, "y-MM-dd");
                console.log($rootScope.dRequest);
            };
            $rootScope.setEndTime = function (sTime) {
                $rootScope.dRequest.endTime = $filter('date')(sTime, "y-MM-dd");
                console.log($rootScope.dRequest);
            };
            ////////////////////////////
            $rootScope.nDeposit = 3;
            $rootScope.nWithdraw = 3;
            //$rootScope.playSound = function(){};
            $rootScope.getMsg = function () {
                //TODO:这里是按时去轮询是否有新的充值提案
                //是否有新短消息
                //有的话就播放声音
                $rootScope.nDeposit++;
                $rootScope.nWithdraw--;
                //last new deposit time 比对后，如果不一样就来声音
                //last new withdraw time 比对后，如果不一样就来声音
            };
            $rootScope.logout = function () {
                localStorageService.remove('User');
                $state.go("login");
            };
            $rootScope.settings = {
                skin: '',
                color: {
                    themeprimary: '#2dc3e8',
                    themesecondary: '#fb6e52',
                    themethirdcolor: '#ffce55',
                    themefourthcolor: '#a0d468',
                    themefifthcolor: '#e75b8d'
                },
                rtl: false,
                fixed: {
                    navbar: false,
                    sidebar: false,
                    breadcrumbs: false,
                    header: false
                }
            };
            if (localStorageService.get('settings')){
                $rootScope.settings = localStorageService.get('settings');
            }            else{
                localStorageService.set('settings', $rootScope.settings);
            }

            $rootScope.$watch('settings', function () {
                if ($rootScope.settings.fixed.header) {
                    $rootScope.settings.fixed.navbar = true;
                    $rootScope.settings.fixed.sidebar = true;
                    $rootScope.settings.fixed.breadcrumbs = true;
                }
                if ($rootScope.settings.fixed.breadcrumbs) {
                    $rootScope.settings.fixed.navbar = true;
                    $rootScope.settings.fixed.sidebar = true;
                }
                if ($rootScope.settings.fixed.sidebar) {
                    $rootScope.settings.fixed.navbar = true;


                    //Slim Scrolling for Sidebar Menu in fix state
                    var position = $rootScope.settings.rtl ? 'right' : 'left';
                    if (!$('.page-sidebar').hasClass('menu-compact')) {
                        $('.sidebar-menu').slimscroll({
                            position: position,
                            size: '3px',
                            color: $rootScope.settings.color.themeprimary,
                            height: $(window).height() - 90,
                        });
                    }
                } else {
                    if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
                        $(".sidebar-menu").slimScroll({destroy: true});
                        $(".sidebar-menu").attr('style', '');
                    }
                }

                localStorageService.set('settings', $rootScope.settings);
            }, true);

            $rootScope.$watch('settings.rtl', function () {
                if ($state.current.name !== "persian.dashboard" && $state.current.name !== "arabic.dashboard") {
                    switchClasses("pull-right", "pull-left");
                    switchClasses("databox-right", "databox-left");
                    switchClasses("item-right", "item-left");
                }

                localStorageService.set('settings', $rootScope.settings);
            }, true);

            $rootScope.$on('$viewContentLoaded',
                function (event, toState, toParams, fromState, fromParams) {
                    if ($rootScope.settings.rtl && $state.current.name !== "persian.dashboard" && $state.current.name != "arabic.dashboard") {
                        switchClasses("pull-right", "pull-left");
                        switchClasses("databox-right", "databox-left");
                        switchClasses("item-right", "item-left");
                    }
                    if ($state.current.name === 'error404') {
                        $('body').addClass('body-404');
                    }
                    if ($state.current.name === 'error500') {
                        $('body').addClass('body-500');
                    }
                    $timeout(function () {
                        if ($rootScope.settings.fixed.sidebar) {
                            //Slim Scrolling for Sidebar Menu in fix state
                            var position = $rootScope.settings.rtl ? 'right' : 'left';
                            if (!$('.page-sidebar').hasClass('menu-compact')) {
                                $('.sidebar-menu').slimscroll({
                                    position: position,
                                    size: '3px',
                                    color: $rootScope.settings.color.themeprimary,
                                    height: $(window).height() - 90,
                                });
                            }
                        } else {
                            if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
                                $(".sidebar-menu").slimScroll({destroy: true});
                                $(".sidebar-menu").attr('style', '');
                            }
                        }
                    }, 500);

                    window.scrollTo(0, 0);
                });
        }
    ]);
app.controller('DashboardCtrl', [
    '$rootScope', '$scope', 'DataService', 'dateService', 'AuthService', '$state',
    function ($rootScope, $scope, DataService, dateService, AuthService, $state) {

        $rootScope.User = AuthService.getCurrentUser();
        if ($scope.User) {
        } else {
            $state.go("login");
        }
        $scope.aReg = [0];
        $scope.bLoadReg = false;
        $scope.nFinish = 0;
        $scope.nTotal = -1;
        $scope.init = function () {
            var dRequest = {};
            var nDay = 6;
            for (var i = 0; i < nDay; i++) {
                var sDate = dateService.stringDate(i);
                dRequest = {"startTime": sDate, "endTime": sDate, "i": i};
                DataService.getUserNum(dRequest).then(function (data) {
                    console.log(data);
                    $scope.aReg[nDay - data.i] = data.n;
                    $scope.nFinish++;
                    if ($scope.nFinish >= nDay) {
                        $scope.bLoadReg = true;
                        console.log("Finish");
                        console.log($scope.aReg);
                        $scope.nFinish = 0;
                    }
                });
            }
            DataService.getUserNum().then(function (data) {
                    $scope.nTotal = data.n;
                }
            );
        };
        angular.element(document).ready(function () {
            //console.log("doc ready");
            //$scope.init();
        });
        ////////////////////
        //获得图表信息：
        //今日注册人数，今日提款人数，今日消费，总会员数
    }
])
    .controller('AccountCtrl', [
        '$rootScope', '$scope', 'AccountDetailService', 'dateService', 'AuthService', '$state', 'ConstService', '$filter','$stateParams',
        function ($rootScope, $scope, AccountDetailService, dateService, AuthService, $state, ConstService, $filter, $stateParams) {
            $scope.pager = {totalItems: 10, maxSize: 10, type: ""};
            $rootScope.dRequest = {"page": 1, "pageSize": 20};
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            $scope.aTitle = [
                ['时间', ''],
                ['用户名', ''],
                ['金额', 'numeric'],
                ['说明', '']
            ];
            if( $stateParams.userName && $stateParams.userName.length >0){
                $rootScope.userName = $stateParams.userName;
                $scope.bDetail =true;
                $rootScope.dRequest.userName =$stateParams.userName;
            }

            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.initBalance = function (type) {
                if(type){
                    $rootScope.dRequest.type = type;
                }

                $scope.aTitle = [
                    ['创建时间/更新时间', ''],
                    ['用户名', ''],
                    ['金额', 'numeric'],
                    ['当前余额', 'numeric'],
                    ['备注', ''],
                    ['订单号', ''],
                    ['说明', '']
                ];

                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    AccountDetailService.getList($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
            $scope.initDeposit = function (bNewOnly) {
                if (bNewOnly != null) {
                    $rootScope.dRequest.status = 1;
                }
                $scope.aStatus = [{i: 0, label: "初始"}, {i: 1, label: "已充值"}];
                $scope.oSearch = ConstService.getSearchBar('depositList');
                $scope.aFs = [{key: "bf", label: "宝付"}, {key: "pnrPay", label: "PNR网银"}, {key: "zfb", label: "支付宝"}];
                $scope.aTitle = [
                    ['创建时间/更新时间', ''],
                    ['用户名', ''],
                    ['订单状态', ''],
                    ['金额', 'numeric'],
                    ['支付方式', ''],
                    ['订单号', ''],
                    ['说明', '']
                ];
                $rootScope.dRequest.type = 0;
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    AccountDetailService.getDeposit($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
            $scope.initWithdraw = function () {
                $scope.oSearch = ConstService.getSearchBar('withdrawList');
                $scope.aTitle = [
                    //序号	用户/手机	金额		/	状态	类型	操作人
                    ['创建时间/更新时间', ''],
                    ['用户', ''],
                    ['金额', 'numeric'],
                    ['提款银行/卡号', ''],

                    ['状态', ''],
                    ['说明', ''],
                    ['操作人', '']
                ];
                $scope.aStatus = [{i: 0, label: '初始'}, {i: 1, label: '审核中'}, {i: 2, label: '已打款'}, {
                    i: 3,
                    label: '已拒绝'
                }];

                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $rootScope.dRequest.type = 1;

                    AccountDetailService.getWithdraw($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
        }
    ])
    .controller('UserCtrl', [
        '$rootScope', '$scope', 'UserService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', '$stateParams',
        function ($rootScope, $scope, UserService, dateService, AuthService, $state, ConstService, $location, $stateParams) {
            $scope.pager = {totalItems: 10, maxSize: 10};
            $rootScope.dRequest = {"page": 1, "pageSize": 20};
            if( $stateParams.userName && $stateParams.userName.length >0){
                $rootScope.userName = $stateParams.userName;
                $scope.bDetail =true;
                $rootScope.dRequest.userName =$stateParams.userName;
            }
            if($stateParams.ip){
                $rootScope.dRequest.ip =$stateParams.ip;
                console.log( $rootScope.dRequest);
            }
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            $scope.goDetail = function (sUserName) {
                $location.path('/UserPage/' + sUserName);
            };
            $scope.mask = function (str) {
                if (!str)return "";
                var aStr = str.split("|");
                var sRes = aStr[0].substring(0, 3) + "****" + aStr[0].substring(7, 11);
                if (aStr.length > 1) {
                    sRes += "|" + aStr[1];
                }
                return sRes;
            };
            $scope.getUserStatus = function (iStatus) {
                return ConstService.getUserStatus(iStatus);
            };
            $scope.aTitle = [
                ['注册时间', ''],
                ['用户名', ''],
                ['真名', ''],
                ['余额', 'numeric'],
                ['返点', ''],
                ['手机', ''],
                ['登陆', ''],
                ['上级列表', '']
            ];
            //$scope.aStatus = [];
            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.pager = {totalItems: 10, maxSize: 10};
            $scope.init = function () {
                $scope.dRequest.orderBy = 0;
                $scope.aStatus = [{i: 0, label: '初始'}, {i: 1, label: '活动'}, {i: 2, label: '冻结'}, {i: 3, label: '删除'}];
                $scope.aType = [{i: 0, label: '用户'}, {i: 1, label: '测试用户'}];
                $scope.requestFunc = function () {
                    $scope.oSearch = ConstService.getSearchBar('userList');
                    $scope.bLoading = true;
                    UserService.getList($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            $scope.bLoading = false;
                        },function(data){
                            alert("查找失败");
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };

            //getBankList
            $scope.initBank = function () {
                $scope.aTitle = [
                    ['时间', ''],
                    ['用户名', ''],
                    ['真名', ''],
                    ['银行', ''],
                    ['开户地', ''],
                    ['银行卡', ''],
                    ['所在城市', '']
                ];
                $scope.oSearch = ConstService.getSearchBar('bankList');
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    UserService.getBankList($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
            $scope.initIP = function () {//ip页面
                $scope.aTitle = [
                    ['时间', ''],
                    ['用户名', ''],
                    ['IP', ''],
                    ['定位', '']
                ];
                $scope.oSearch = ConstService.getSearchBar('ip');
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.oSearch = ConstService.getSearchBar('loginLog');
                    UserService.getLoginLog($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
            $scope.initIllegal = function () {//非法用户
                $scope.aTitle = [
                    ['时间', ''],
                    ['用户名', ''],
                    ['信息', '']
                ];
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.oSearch = ConstService.getSearchBar('illegal');
                    UserService.getIllegalList($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
        }
    ])
    .controller('UserDetailCtrl', [
        '$rootScope', '$scope', 'UserService', 'dateService', 'AuthService', '$state', 'ConstService', '$stateParams',
        function ($rootScope, $scope, UserService, dateService, AuthService, $state, ConstService, $stateParams) {
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            $scope.getRegFrom = function (i) {
                return ConstService.getRegFrom(i);
            };
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            console.log($stateParams);
            if( $stateParams.userName!=null && $stateParams.userName.length >0){
                $rootScope.userName = $stateParams.userName;
            }
            $scope.getUserStatus = function (iStatus) {
                return ConstService.getUserStatus(iStatus);
            };
            //$scope.aStatus = [];
            $scope.init = function () {//用户详情单页，在这里加载用户的信息
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    var request = {
                        username: $stateParams.userName
                    };
                    UserService.getDetail(request).then(
                        function (data) {
                            $scope.data = data;
                            if (!$scope.data.member.username) {
                                $scope.data.member = eval('(' + data.member + ')');
                            }
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
            angular.element(document).ready(function () {
                $scope.init();
            });
        }
    ])

    .controller('OrderCtrl', [
        '$rootScope', '$scope', 'OrderService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', '$stateParams',
        function ($rootScope, $scope, OrderService, dateService, AuthService, $state, ConstService, $location, $stateParams) {
            $scope.pager = {totalItems: 10, maxSize: 10};
            $rootScope.dRequest = {"page": 1, "pageSize": 20};
            if( $stateParams.userName && $stateParams.userName.length >0){
                $rootScope.userName = $stateParams.userName;
                $scope.bDetail =true;
                $rootScope.dRequest.userName =$stateParams.userName;
            }
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            $scope.aTitle = [
                ['时间', ''],
                ['期次', ''],
                ['用户', ''],
                ['金额', ''],
                ['中奖金额', ''],
                ['彩种', ''],
                ['玩法', ''],
                ['订单号', ''],
                ['状态', ''],
                ['投注模式', '']
            ];
            //$scope.aStatus = [];
            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.pager = {totalItems: 10, maxSize: 10};
            $scope.init = function () {
                $scope.dRequest.orderBy = 0;
                $scope.aStatus = [{i: 0, label: '初始'}, {i: 1, label: '活动'}, {i: 2, label: '冻结'}, {i: 3, label: '删除'}];
                $scope.aType = [{i: 0, label: '用户'}, {i: 1, label: '测试用户'}];
                $scope.requestFunc = function () {
                    $scope.oSearch = ConstService.getSearchBar('orderList');
                    $scope.bLoading = true;
                    OrderService.getList($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
        }
    ])
    .controller('OrderDetailCtrl', [
        '$rootScope', '$scope', 'OrderService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', '$stateParams',
        function ($rootScope, $scope, OrderService, dateService, AuthService, $state, ConstService, $location, $stateParams) {
            $scope.pager = {totalItems: 10, maxSize: 10};
            $rootScope.dRequest = {"page": 1, "pageSize": 20};
            if( $stateParams.orderId && $stateParams.orderId.length >0){
                $scope.orderId = $stateParams.orderId;
                $scope.bDetail =true;
                $rootScope.dRequest.orderId =$stateParams.orderId;
            }
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.pager = {totalItems: 10, maxSize: 10};
            $scope.init = function () {
                $scope.dRequest.orderBy = 0;
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    OrderService.getDetail($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.data = data;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
        }
    ]);;
angular.module('app')
  .directive('loadingContainer', function () {
      return {
          restrict: 'AC',
          link: function (scope, el, attrs) {
              el.removeClass('app-loading');
              scope.$on('$stateChangeStart', function (event) {
                  el.addClass('app-loading');
              });
              scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
                  event.targetScope.$watch('$viewContentLoaded', function() {
                      el.removeClass('app-loading ');
                  });
              });
          }
      };
  });
//Chat Toggle Link
angular.module('app')
    .directive('skinChanger', ['$rootScope', '$filter','$state', '$stateParams', function ($rootScope, $filter, $state, $stateParams) {
        return {
            restrict: 'AC',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    setTimeout(function () {
                        $rootScope.$apply(function () {
                            var skincolor = $filter('filter')(scope.skins, function (d) { return d.skin === el.attr('rel'); })[0];
                            $rootScope.settings.skin = skincolor.skin;
                            $rootScope.settings.color = skincolor.color;
                            $state.transitionTo($state.current, $stateParams, {
                                reload: true,
                                inherit: false,
                                notify: true
                            });
                        });
                    }, 100);
                });
                scope.skins = [
                    {
                        skin: 'assets/css/skins/blue.min.css',
                        color: {
                            themeprimary: '#5db2ff',
                            themesecondary: '#ed4e2a',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/azure.min.css',
                        color: {
                            themeprimary: '#2dc3e8',
                            themesecondary: '#fb6e52',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/black.min.css',
                        color: {
                            themeprimary: '#474544',
                            themesecondary: '#d73d32',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/darkblue.min.css',
                        color: {
                            themeprimary: '#0072c6',
                            themesecondary: '#fb6e52',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/darkred.min.css',
                        color: {
                            themeprimary: '#ac193d',
                            themesecondary: '#7bd148',
                            themethirdcolor: '#5db2ff',
                            themefourthcolor: '#e75b8d',
                            themefifthcolor: '#ffce55'
                        }
                    },
                    {
                        skin: 'assets/css/skins/deepblue.min.css',
                        color: {
                            themeprimary: '#001940',
                            themesecondary: '#d73d32',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/deepblue.min.css',
                        color: {
                            themeprimary: '#001940',
                            themesecondary: '#d73d32',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/gray.min.css',
                        color: {
                            themeprimary: '#585858',
                            themesecondary: '#fb6e52',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/green.min.css',
                        color: {
                            themeprimary: '#53a93f',
                            themesecondary: '#ed4e2a',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/orange.min.css',
                        color: {
                            themeprimary: '#ff8f32',
                            themesecondary: '#7bd148',
                            themethirdcolor: '#5db2ff',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/pink.min.css',
                        color: {
                            themeprimary: '#cc324b',
                            themesecondary: '#8cc474',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#e75b8d'
                        }
                    },
                    {
                        skin: 'assets/css/skins/purple.min.css',
                        color: {
                            themeprimary: '#8c0095',
                            themesecondary: '#ffce55',
                            themethirdcolor: '#e75b8d',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#fb6e52'
                        }
                    },
                    {
                        skin: 'assets/css/skins/teal.min.css',
                        color: {
                            themeprimary: '#03b3b2',
                            themesecondary: '#ed4e2a',
                            themethirdcolor: '#ffce55',
                            themefourthcolor: '#a0d468',
                            themefifthcolor: '#fb6e52'
                        }
                    }
                ];
                
            }
        };
    }]);
//Sidebar Menu Handle
angular.module('app')
    .directive('sidebarMenu', function() {
        return {
            restrict: 'AC',
            link: function (scope, el, attr) {
                el.find('li.active').parents('li').addClass('active open');

                el.on('click', 'a', function (e) {
                    e.preventDefault();
                    var isCompact = $("#sidebar").hasClass("menu-compact");
                    var menuLink = $(e.target);
                    if ($(e.target).is('span'))
                        menuLink = $(e.target).closest('a');
                    if (!menuLink || menuLink.length == 0)
                        return;
                    if (!menuLink.hasClass("menu-dropdown")) {
                        if (isCompact && menuLink.get(0).parentNode.parentNode == this) {
                            var menuText = menuLink.find(".menu-text").get(0);
                            if (e.target != menuText && !$.contains(menuText, e.target)) {
                                return false;
                            }
                        }
                        return;
                    }
                    var submenu = menuLink.next().get(0);
                    if (!$(submenu).is(":visible")) {
                        var c = $(submenu.parentNode).closest("ul");
                        if (isCompact && c.hasClass("sidebar-menu"))
                            return;
                        c.find("* > .open > .submenu")
                            .each(function() {
                                if (this != submenu && !$(this.parentNode).hasClass("active"))
                                    $(this).slideUp(200).parent().removeClass("open");
                            });
                    }
                    if (isCompact && $(submenu.parentNode.parentNode).hasClass("sidebar-menu"))
                        return false;
                    $(submenu).slideToggle(200).parent().toggleClass("open");
                    return false;
                });
            }
        };
    });


angular.module('app')
    .directive('fullscreen', function() {
            return {
                restrict: 'AC',
                template: '<i class="glyphicon glyphicon-fullscreen"></i>',
                link: function(scope, el, attr) {
                    el.on('click', function() {
                        var element = document.documentElement;
                        if (!$('body')
                            .hasClass("full-screen")) {

                            $('body')
                                .addClass("full-screen");
                            $('#fullscreen-toggler')
                                .addClass("active");
                            if (element.requestFullscreen) {
                                element.requestFullscreen();
                            } else if (element.mozRequestFullScreen) {
                                element.mozRequestFullScreen();
                            } else if (element.webkitRequestFullscreen) {
                                element.webkitRequestFullscreen();
                            } else if (element.msRequestFullscreen) {
                                element.msRequestFullscreen();
                            }

                        } else {

                            $('body').removeClass("full-screen");
                            el.removeClass("active");

                            if (document.exitFullscreen) {
                                document.exitFullscreen();
                            } else if (document.mozCancelFullScreen) {
                                document.mozCancelFullScreen();
                            } else if (document.webkitExitFullscreen) {
                                document.webkitExitFullscreen();
                            }

                        }
                    });
                }
            };
        }
    );

angular.module('app')
    .directive('refresh', [
        '$rootScope', '$state', '$stateParams',
        function($rootScope, $state, $stateParams) {
            return {
                restrict: 'AC',
                template: '<i class="glyphicon glyphicon-refresh"></i>',
                link: function(scope, el, attr) {
                    el.on('click', function() {
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    });
                }
            };
        }
    ]);

angular.module('app')
    .directive('sidebarToggler', function() {
            return {
                restrict: 'AC',
                template: '<i class="fa fa-arrows-h"></i>',
                link: function(scope, el, attr) {
                    el.on('click', function() {
                        $("#sidebar").toggleClass("hide");
                        el.toggleClass("active");
                        return false;
                    });
                }
            };
        }
    );

angular.module('app')
    .directive('pageTitle', [
        '$rootScope', '$timeout',
        function($rootScope, $timeout) {
            return {
                link: function(scope, element) {

                    var listener = function(event, toState) {
                        var title = 'Default Title';
                        if (toState.ncyBreadcrumb && toState.ncyBreadcrumb.label) title = toState.ncyBreadcrumb.label;
                        $timeout(function() {
                            element.text(title);
                        }, 0, false);
                    };
                    $rootScope.$on('$stateChangeSuccess', listener);
                }
            };
        }
    ]);

angular.module('app')
    .directive('headerTitle', [
        '$rootScope', '$timeout',
        function ($rootScope, $timeout) {
            return {
                link: function (scope, element) {

                    var listener = function (event, toState) {
                        var title = 'Default Title';
                        var description = '';
                        if (toState.ncyBreadcrumb && toState.ncyBreadcrumb.label) title = toState.ncyBreadcrumb.label;
                        if (toState.ncyBreadcrumb && toState.ncyBreadcrumb.description) description = toState.ncyBreadcrumb.description;
                        $timeout(function () {
                            if(description == '')
                                element.text(title);
                            else 
                                element.html(title + ' <small> <i class="fa fa-angle-right"> </i> '+ description + ' </small>');
                        }, 0, false);
                    };
                    $rootScope.$on('$stateChangeSuccess', listener);
                }
            };
        }
    ]);
//Sidebar Collapse
angular.module('app')
    .directive('sidebarCollapse', function () {
        return {
            restrict: 'AC',
            template: '<i class="collapse-icon fa fa-bars"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    if (!$('#sidebar').is(':visible'))
                        $("#sidebar").toggleClass("hide");
                    $("#sidebar").toggleClass("menu-compact");
                    $(".sidebar-collapse").toggleClass("active");
                    var isCompact = $("#sidebar").hasClass("menu-compact");

                    if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
                        $(".sidebar-menu").slimScroll({ destroy: true });
                        $(".sidebar-menu").attr('style', '');
                    }
                    if (isCompact) {
                        $(".open > .submenu")
                            .removeClass("open");
                    } else {
                        if ($('.page-sidebar').hasClass('sidebar-fixed')) {
                            var position = (readCookie("rtl-support") || location.pathname == "/index-rtl-fa.html" || location.pathname == "/index-rtl-ar.html") ? 'right' : 'left';
                            $('.sidebar-menu').slimscroll({
                                height: $(window).height() - 90,
                                position: position,
                                size: '3px',
                                color: themeprimary
                            });
                        }
                    }
                    //Slim Scroll Handle
                });
            }
        };
    });

//Setting
angular.module('app')
    .directive('setting', function () {
        return {
            restrict: 'AC',
            template: '<a id="btn-setting" title="Setting" href="#"><i class="icon glyphicon glyphicon-cog"></i></a>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    $('.navbar-account').toggleClass('setting-open');
                });
            }
        };
    });

//Chat Toggle Link
angular.module('app')
    .directive('chatLink', function () {
        return {
            restrict: 'AC',
            template: '<i class="icon glyphicon glyphicon-comment"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    $('.page-chatbar').toggleClass('open');
                    el.toggleClass('wave').toggleClass('in');
                    el.parent().toggleClass('open');
                });
            }
        };
    });


angular.module('app')
    .directive('pageChatbar', ['$cookies', function ($cookies) {
        return {
            restrict: 'AC',
            link: function (scope, el, attr) {
                var additionalHeight = 0;
                if ($(window).width() < 531)
                    additionalHeight = 45;
                var position = ($cookies.rtlSupport || location.pathname == "/index-rtl-fa.html" || location.pathname == "/index-rtl-ar.html") ? 'right' : 'left';
                $('.chatbar-messages .messages-list').slimscroll({
                    position: position,
                    size: '4px',
                    color: scope.settings.color.themeprimary,
                    height: $(window).height() - (250 + additionalHeight),
                });
                $('.chatbar-contacts .contacts-list').slimscroll({
                    position: position,
                    size: '4px',
                    color: scope.settings.color.themeprimary,
                    height: $(window).height() - (86 + additionalHeight),
                });
                el.on('click', '.chatbar-contacts .contact', function () {
                    el.find('.chatbar-contacts').hide();
                    el.find('.chatbar-messages').show();
                });

                el.on('click', '.chatbar-messages .back', function () {
                    el.find('.chatbar-messages').hide();
                    el.find('.chatbar-contacts').show();
                });
            }
        };
    }]);
//Maximize Widget
angular.module('app')
    .directive('widgetMaximize', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-expand"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    var widget = el.parents(".widget").eq(0);
                    var button = el.find("i").eq(0);
                    var compress = "fa-compress";
                    var expand = "fa-expand";
                    if (widget.hasClass("maximized")) {
                        if (button) {
                            button.addClass(expand).removeClass(compress);
                        }
                        widget.removeClass("maximized");
                        widget.find(".widget-body").css("height", "auto");
                    } else {
                        if (button) {
                            button.addClass(compress).removeClass(expand);
                        }
                        widget.addClass("maximized");
                        if (widget) {
                            var windowHeight = $(window).height();
                            var headerHeight = widget.find(".widget-header").height();
                            widget.find(".widget-body").height(windowHeight - headerHeight);
                        }
                    }
                });
            }
        };
    });

//Collapse Widget
angular.module('app')
    .directive('widgetCollapse', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-minus"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    var widget = el.parents(".widget").eq(0);
                    var body = widget.find(".widget-body");
                    var button = el.find("i");
                    var down = "fa-plus";
                    var up = "fa-minus";
                    var slidedowninterval = 300;
                    var slideupinterval = 200;
                    if (widget.hasClass("collapsed")) {
                        if (button) {
                            button.addClass(up).removeClass(down);
                        }
                        widget.removeClass("collapsed");
                        body.slideUp(0, function () {
                            body.slideDown(slidedowninterval);
                        });
                    } else {
                        if (button) {
                            button.addClass(down)
                                .removeClass(up);
                        }
                        body.slideUp(slideupinterval, function () {
                            widget.addClass("collapsed");
                        });
                    }
                });
            }
        };
    });

//Expand Widget
angular.module('app')
    .directive('widgetExpand', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-plus"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    var widget = el.parents(".widget").eq(0);
                    var body = widget.find(".widget-body");
                    var button = el.find("i");
                    var down = "fa-plus";
                    var up = "fa-minus";
                    var slidedowninterval = 300;
                    var slideupinterval = 200;
                    if (widget.hasClass("collapsed")) {
                        if (button) {
                            button.addClass(up).removeClass(down);
                        }
                        widget.removeClass("collapsed");
                        body.slideUp(0, function () {
                            body.slideDown(slidedowninterval);
                        });
                    } else {
                        if (button) {
                            button.addClass(down)
                                .removeClass(up);
                        }
                        body.slideUp(slideupinterval, function () {
                            widget.addClass("collapsed");
                        });
                    }
                });
            }
        };
    });

//Dispose Widget
angular.module('app')
    .directive('widgetDispose', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-times"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    var widget = el.parents(".widget").eq(0);
                    var disposeinterval = 300;
                    widget.hide(disposeinterval, function () {
                        widget.remove();
                    });
                });
            }
        };
    });

//Config Widget
angular.module('app')
    .directive('widgetConfig', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-cog"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                   //Do what you intend for configing widgets
                });
            }
        };
    });

//Config Widget
angular.module('app')
    .directive('widgetRefresh', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-undo"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    //Refresh widget content
                });
            }
        };
    });
/**
 * 服务
 */
angular.module('app')
    //数据服务
    .factory('DataService',
    ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',"$state",
        function ($http, $q, ConfigService, localStorageService, AuthService, $state) {
            var dataService = {};
            var User;
            dataService.getUserNum = function (dRequest) {
                //这里返回某个时间段的会员数
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                    + User.sid + '?do=getAdminList&json=true&pageSize=1&isCount=1',
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.n) {//有数据
                            if(dRequest){
                                data.i = dRequest.i;
                            }

                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            //查询某个时间段的存款总额，和存款人数
            dataService.getDepositNum = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                    + User.sid + '?do=getAdminList&json=true',
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .then(
                    function (data, status, header, config) {
                        if (data == null) {
                            console.log("empty");
                            deferred.reject("网络出错，请重试");
                        } else if (data.pageId) {//有数据
                            console.log(data.pageContent);
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            //查询某个时间段的出款总额，和出款会员数
            dataService.getCashOutNum = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                    + User.sid + '?do=getAdminList&json=true',
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .success(
                    function (data, status, header, config) {
                        if (data == null) {
                            console.log("empty");
                            deferred.reject("网络出错，请重试");
                        } else if (data.pageId) {//有数据
                            console.log(data.pageContent);
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    }
                );
                return deferred.promise;
            };

            return dataService;
        }])
    //财务
    .factory('AccountDetailService',
    ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
        function ($http, $q, ConfigService, localStorageService, AuthService) {
            var accountDetailService = {};
            var User;
            //提款申请
            accountDetailService.getWithdraw = function (dRequest) {
                //status : 状态
                //type: 0 表示存款，1表示出款申请
                //这里返回某个时间段的会员数
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/DepositRecordsServlet;jsessionid='
                    + User.sid + '?do=list&json=true',
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.pageTotal) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            //查询存款提案
            accountDetailService.getDeposit = function (dRequest) {
                //status : 状态
                //type: 0 表示存款，1表示出款申请
                //这里返回某个时间段的会员数
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/DepositRecordsServlet;jsessionid='
                    + User.sid + '?do=list&json=true',
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.pageTotal) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            /*
             page=页码
             可选参数
             pageSize=每页显示数量 默认为50
             userName=用户名
             startTime=开始时间 YYYY-mm-dd
             endTime=结束时间 YYYY-mm-dd
             type=类型
             　　(101=第三支付
             　　102=银行转账
             　　103=投注返点
             　　104=他人赠与
             　　105=返奖
             　　106=注册送28
             　　
             　　201=投注扣款
             　　202=提现
             　　204=转账
             　　205=退奖)
             */
            accountDetailService.getList = function (dRequest) {
                //这里返回某种订单的列表
                User = AuthService.getCurrentUser();
                console.log(User);
                var sUrl = ConfigService.getHost() + '/manages/AccountDetailServlet' +
                    ';jsessionid=' + User.sid
                    + '?do=list&json=true';

                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: sUrl,
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .then(
                    function (dataRecv, status, header, config) {
                        data = dataRecv.data;
                        if (data == null) {
                            console.log("empty");
                            deferred.reject("网络出错，请重试");
                        } else if (data.pageId) {//有数据
                            console.log(data.pageContent);
                            deferred.resolve(data);
                        } else {
                            console.log(data);
                            deferred.reject(data);
                        }
                    },
                    function (data) {
                        AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            return accountDetailService;
        }])
    .factory('UserService',
    ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
        function ($http, $q, ConfigService, localStorageService, AuthService) {
            var userService = {};
            var User;
            //这里返回用户列表
            userService.getList = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                    + User.sid + '?do=getAdminList&json=true',
                   // params: dRequest,
                    data:dRequest,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.itemTotal) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        //AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            //这里返回银行卡信息
            userService.getBankList = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    ///manages/UserinfoServlet?do=bankList
                    url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                    + User.sid + '?do=bankList&json=true',
                    params: dRequest,
                    headers: {'Content-Type': 'text/html;charset=UTF-8'}
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.itemTotal) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            //139.162.10.174:8158/manages/UserinfoServlet?do=illegalList
            //非法请求
            userService.getIllegalList = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                    + User.sid + '?do=illegalList&json=true',
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.itemTotal) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            //http://139.162.10.174:8158/manages/UserinfoServlet?do=loginLog
            //用户IP列表
            userService.getLoginLog = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                    + User.sid + '?do=loginLog&json=true',
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.itemTotal) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            //user detail
            //get detail by id
            userService.getDetail = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                var sDo = "getMemberByUserId";

                if(!dRequest.id ||  dRequest.id== ""){
                    sDo = "getMemberByUserName";
                }
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                    + User.sid + '?do='+sDo+'&json=true',
                    params: dRequest,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        console.log(data);
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.member) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        deferred.reject("网络出错，请重试");
                    }
                );
                return deferred.promise;
            };
            return userService;
        }])
.  factory('OrderService',
    ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
        function ($http, $q, ConfigService, localStorageService, AuthService) {
            var orderService = {};
            var User;
            //这里返回列表
            orderService.getList = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/OrderQueryCenter.action;jsessionid='
                    + User.sid + '?do=list&json=true',
                    // params: dRequest,
                    data:dRequest,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                    }
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.itemTotal) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        //AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            orderService.getDetail = function (dRequest) {
                User = AuthService.getCurrentUser();
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/manages/OrderQueryCenter.action;jsessionid='
                    + User.sid + '?do=orderDetail&json=true&orderjson=true',
                    // params: dRequest,
                    data:dRequest,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                    }
                })
                    .then(
                    function (resp, status, header, config) {
                        data = resp.data;
                        if (data == null) {
                            deferred.reject("网络出错，请重试");
                        } else if (data.id) {//有数据
                            deferred.resolve(data);
                        } else
                            deferred.reject(data);
                    },
                    function (data) {
                        //AuthService.relogin();
                        deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            return orderService;
        }]);
/**
 * 服务
 */
angular.module('app')
    //权限管理
    .factory('AuthService', ['$http', '$q', 'ConfigService', 'localStorageService', '$state',
        function ($http, $q, ConfigService, localStorageService, $state) {
            var authService = {};
            authService.login = function (credentials) {
                var deferred = $q.defer();
                var data = {"username": credentials.username, "password": credentials.password, "wid":'139.162.10.174'} ;
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '//login.action?do=login',
                    data: 'adminName=' + credentials.username + '&password=' + credentials.password +
                    '&verifyCode=' + credentials.verify +'&wid=' +data.wid,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .success(
                    function (data, status, header, config) {
                        if (data == null) {
                            deferred.reject("网络出错,请稍后再试");
                        } else {
                            if (data.length > 100) {
                                deferred.reject("登陆出错");
                            }
                            var aData;
                            aData = data.split("|");
                            console.log(aData);
                            var msg = aData[1];
                            if (aData[2]) {//登陆成功
                                var userData = {"sid": aData[2], "user": credentials.username};
                                var str = JSON.stringify(userData, null, ' ');
                                localStorageService.set('User', str);
                                deferred.resolve(userData);
                            } else
                                deferred.reject(msg);
                        }
                    }
                );
                return deferred.promise;
            };
            authService.getCurrentUser = function () {
                var User = JSON.parse(localStorageService.get('User'));
                if (User) {
                    return User;
                } else{
                    authService.relogin();
                    return null;
                }
            };
            authService.checkUser = function (dRequest) {
                var deferred = $q.defer();
                var data = {"username": dRequest.username, "sid": dRequest.sid};
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/v1/user/checkuser',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    },
                    params: dRequest
                })
                    .success(
                    function (data, status, header, config) {
                        if (data.sid) {//登陆成功
                            deferred.resolve(userData);
                        } else
                            deferred.reject(data);
                    }
                );
                return deferred.promise;
            };
            authService.settleError = function (msg) {
                if (msg == '登陆超时' || msg == '请登录') {
                    localStorageService.remove('User');
                    $state.go('login');
                }
                alert(msg);
            };
            authService.isAuthenticated = function () {
                return !!0;
            };
            authService.relogin = function () {
                localStorageService.remove('User');
                $state.go('login');
            };
            authService.isAuthorized = function (authorizedRoles) {
                if (!angular.isArray(authorizedRoles)) {
                    authorizedRoles = [authorizedRoles];
                }
                return true; //(authService.isAuthenticated() &&authorizedRoles.indexOf(Session.userRole) !== -1);
            };
            return authService;
        }]);
/**
 * 这里放一些常量，和字典
 */
angular.module('app')
    .service('ConfigService', [function () {
        var hostURL = "http://139.162.10.174:8358";
        //http://139.162.10.174:8158/login.action?do=login&adminName=administrator&password=123456&verifyCode=200ceb26807d6bf99fd6f4f0d1ca54d4
        var service = {
            getHost: function () {
                return hostURL;
            }
        };
        return service;
    }])
    .service('ConstService', [function () {
        var oSearchBarConfig = {
            //这个配置决定了某个页面上面search栏的组合
            "orderList": {
                "orderNo":1, "autoOrderNo":1, "issue": 1,
                "lotteryCode": 1, "status": 1, "betAA": 1, "bonAA": 1,"startTime": 1
            },
            "userList": {
                "realname": 1, "upusername": 1, "status": 1,
                "type": 1, "orderBy": 1, "registerTime": 1, "rebate_info": 1,"mobile":true,
                "startTime":true
            },
            'depositList': {
                "status" : true, 'startAmount':true, 'fs':true, 'startTime':true
            },
            'withdrawList': {
                "status" : true, 'startAmount':true, 'startTime':true
            },
            'OQuery': {
                "betAA":true, "bonAA":true, "autoOrderId":true,
                "startTime":true,"orderNo":true,"lotteryCode":true,"issue":true,
                "status" : true, "type":true
            },

            'bankList': {
                "startTime":true, "mobile":true
            },
            'issueList': {
                "lotteryCode":true,"issue":true, "status" : true
            },//期次

            'loginLog': {
                "ip":true, "startTime":true
            },
            'illegal':{
                "startTime":true
            }

        };
        var constService = {
            getUserStatus: function (iStatus) {
                var userStatus = ["", "<font color='blue'>活动</font>", "<font color='red'>已冻结</font>", "<font color='red'>删除</font>"];
                return userStatus[iStatus];
            },
            getRegFrom: function (i) {
                var aRegFrom = ["连接注册", "其他1", "代理开通", ""];
                return aRegFrom[i];
            },
            //传入page的名字，返回搜索bar的配置（即可用的参数，可以冗余）
            getSearchBar: function (sPage) {
                return oSearchBarConfig[sPage];
            }
        };
        return constService;
    }])
    .service('dateService', [function () {
        var dateService = {};
        dateService.stringMonth = function (i) {
            var d = ( new Date()).valueOf();
            var date = new Date(d);
            while (i > 0) {
                date = new Date(d);
                var tmp = new Date(date.getFullYear(), date.getMonth(), 0);
                d -= tmp.getDate() * 3600 * 24 * 1000;
                i--;
            }
            date = new Date(d);
            var Y = date.getFullYear() + '-';
            var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            return (Y + M );
        };
        dateService.stringDate = function (i) {
            var t = (new Date()).valueOf() - i * 3600 * 24 * 1000;
            var date = new Date(t);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()  );
            return (Y + M + D);
        };
        dateService.stringTime = function (i) {
            var t = (new Date()).valueOf() - i * 3600 * 24 * 1000;
            var date = new Date(t);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()  ) + ' ';
            hms = '23:59:59';
            return (Y + M + D + hms);
        };
        dateService.getDateArray = function (n) {
            var arrDate = new Array(n);
            arrDate[0] = '今天';
            arrDate[1] = '昨天';
            for (i = 2; i < n; i++) {
                var t = (new Date()).valueOf() - i * 3600 * 24 * 1000;
                var date = new Date(t);
                M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '月';
                D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()  ) + '日';
                arrDate[i] = M + D;
            }
            return arrDate;
        };
        return dateService;
        }])
        //这里设置httpProvider,重写transformRequest，使用data
        .config(['$httpProvider', function ($httpProvider) {
            // Intercept POST requests, convert to standard form encoding
            $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.transformRequest.unshift(function (data, headersGetter) {
                var key, result = [];

                if (typeof data === "string")
                    return data;

                for (key in data) {
                    if (data.hasOwnProperty(key))
                        result.push(encodeURIComponent(key) + "=" + encodeURIComponent(data[key]));
                }
                return result.join("&");
            });
            $httpProvider.defaults.transformRequest = [function (data) {
                /**
                 * The workhorse; converts an object to x-www-form-urlencoded serialization.
                 * @param {Object} obj
                 * @return {String}
                 */
                var param = function (obj) {
                    var query = '';
                    var name, value, fullSubName, subName, subValue, innerObj, i;

                    for (name in obj) {
                        value = obj[name];

                        if (value instanceof Array) {
                            for (i = 0; i < value.length; ++i) {
                                subValue = value[i];
                                fullSubName = name + '[' + i + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        } else if (value instanceof Object) {
                            for (subName in value) {
                                subValue = value[subName];
                                fullSubName = name + '[' + subName + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        } else if (value !== undefined && value !== null) {
                            query += encodeURIComponent(name) + '='
                                + encodeURIComponent(value) + '&';
                        }
                    }

                    return query.length ? query.substr(0, query.length - 1) : query;
                };

                return angular.isObject(data) && String(data) !== '[object File]'
                    ? param(data)
                    : data;
            }];
        }]);
