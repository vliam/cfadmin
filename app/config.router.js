'use strict';
angular.module('app')
    .run(
    [
        '$rootScope', '$state', '$stateParams',
        function ($rootScope, $state, $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }
    ]
)
    .config(
    [
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider
                .otherwise('/app/dashboard');
            $stateProvider
                .state('app', {
                    abstract: true,
                    url: '/app',
                    templateUrl: 'views/layout.html'
                })
                .state('app.dashboard', {
                    url: '/dashboard',
                    templateUrl: 'views/allReport.html',
                    ncyBreadcrumb: {
                        label: '数据统计 Statistics',
                        description: ''
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'lib/jquery/charts/sparkline/jquery.sparkline.js',
                                        'lib/jquery/charts/chartjs/chart.js'
                                    ]
                                });
                            }
                        ]
                    }
                })

                //------------------------------------------------///
                .state('app.allReport', {
                    url: '/allReport',
                    templateUrl: 'views/dashboard.html',
                    ncyBreadcrumb: {
                        label: '全站日结报表 Daily Report'
                    }
                })

                .state('app.userReport', {
                    url: '/userReport/:userName?',
                    templateUrl: 'views/userReport.html',
                    ncyBreadcrumb: {
                        label: '用户日结User Daily'
                    }
                })
            
                .state('app.credit', {
                    url: '/credit',
                    templateUrl: 'views/credit.html',
                    ncyBreadcrumb: {
                        label: '转账额度Credit'
                    }
                })
                ///////////////////////////////////////////

                .state('app.business', {
                    url: '/business/:userNameParams?',
                    templateUrl: 'views/businessChanges.html',
                    ncyBreadcrumb: {
                        label: '账户变动Account Details'
                    }
                })
                .state('app.deposit', {
                    url: '/deposit/:userNameParams?',
                    templateUrl: 'views/deposit.html',
                    ncyBreadcrumb: {
                        label: '充值记录Recharge'
                    }
                })
                .state('app.withdraw', {
                    url: '/withdraw/:userNameParams?',
                    templateUrl: 'views/withdraw.html',
                    ncyBreadcrumb: {
                        label: '提现记录WithDraw'
                    }
                })
                //////////////////////////////////////
                .state('app.user', {
                    url: '/user',
                    templateUrl: 'views/user.html',
                    ncyBreadcrumb: {
                        label: '用户列表Users'
                    }
                })
                //////////////////////////////////////
                .state('app.admin', {
                    url: '/admin',
                    templateUrl: 'views/admin.html',
                    ncyBreadcrumb: {
                        label: '管理员列表AdminList'
                    }
                })
                .state('app.adminDetail', {
                    url: '/adminDetail/:adminName?',
                    templateUrl: 'views/adminDetail.html',
                    ncyBreadcrumb: {
                        label: '管理员详情AdminDetailList'
                    }
                })
                .state('app.adminUpdate', {
                    url: '/adminUpdate/:id?',
                    templateUrl: 'views/adminUpdate.html',
                    ncyBreadcrumb: {
                        label: '管理员修改AdminUpdate'
                    }
                })

                ////////////////////////////////////
               /* .state('app.illegal', {
                    url: '/illegal',
                    templateUrl: 'views/illegal.html',
                    ncyBreadcrumb: {
                        label: 'illegalRequest'
                    }
                })
                .state('app.loginLog', {
                    url: '/loginLog/:ip?',
                    templateUrl: 'views/LoginLog.html',
                    ncyBreadcrumb: {
                        label: 'IP Log'
                    }
                })*/
                .state('app.history', {
                    url: '/history/:gameNumber?',
                    templateUrl: 'views/historyList.html',
                    ncyBreadcrumb: {
                        label: '历史记录 History'
                    }
                })
                /////////////////////////////////////////////
                .state('login', {
                    url: '/login',
                    templateUrl: 'views/login.html',
                    ncyBreadcrumb: {
                        label: 'Login'
                    },
                    resolve: {
                        deps: [
                            '$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
                                    serie: true,
                                    files: [
                                        'app/controllers/login.js'
                                    ]
                                });
                            }
                        ]
                    }
                })
                ////////////////////////////////////////////
               /* .state('userPage', {
                    url: '/userPage/{userName}',//这里是子类
                    templateUrl: 'views/userDetail.html',
                    ncyBreadcrumb: {
                        label: 'UserDetail'
                    }
                })
                .state('loginLog', {
                    url: '/loginLog/{userName}',
                    templateUrl: 'views/LoginLog.html',
                    ncyBreadcrumb: {
                        label: '个人IP分析'
                    }
                })
*/
                .state('user999', {
                    url: '/user999/:userName',
                    templateUrl: 'views/user999.html',
                    ncyBreadcrumb: {
                        label: '账户详情 AccountDetail'
                    }
                })
                .state('app.orderList', {
                    url: '/orderList/:inputParams?',
                    templateUrl: 'views/orderList.html',
                    ncyBreadcrumb: {
                        label:  '投注记录 OrderList'
                    }
                })
                .state('app.orderWin', {
                    url: '/orderWin',
                    templateUrl: 'views/orderWin.html',
                    ncyBreadcrumb: {
                        label: '获奖订单 Order Win'
                    }
                })
                .state('orderList', {
                    url: '/order/:userName',
                    templateUrl: 'views/orderList.html',
                    ncyBreadcrumb: {
                        label: '投注记录 OrderList'
                    }
                })
                .state('app.qtOrderList', {
                    url: '/qtOrder',
                    templateUrl: 'views/qtOrder.html',
                    ncyBreadcrumb: {
                        label:  'QT Order'
                    }
                })
                .state('app.pending', {
                    url: '/pending',
                    templateUrl: 'views/pendingOrder.html',
                    ncyBreadcrumb: {
                        label:  '未处理订单Pending Order'
                    }
                })
              /*  .state('orderDetail', {
                    url: '/orderDetail/:orderId',
                    templateUrl: 'views/orderDetail.html',
                    ncyBreadcrumb: {
                        label: 'OrderDetail'
                    }
                })*/
                ////////////////////////////////////////

            ;
        }
    ]
);
