'use strict';

angular.module('app')
    .controller('AppCtrl', [
        '$rootScope', '$scope', 'localStorageService', '$modal', '$state',
        '$timeout', '$filter', '$location', 'UserService','ReportService','T',
        function
            ($rootScope, $scope, localStorageService, $modal, $state,
             $timeout, $filter, $location, UserService, ReportService, T) {
            /*  $rootScope.reRequest = function(page){
             if(page!=null){
             $scope.dRequest.page = 1;
             $scope.requestFunc();
             }else{
             $scope.requestFunc();
             }
             };*/
            
            $rootScope.reportHistorySearchGameIndex = 0; 

            $rootScope.oi18n  = {txt: T.txt};
            $rootScope.search = function (uname, bMain) {
                UserService.getDetail({username: uname}).then(
                    function (data) {
                        var regExp = new RegExp("/[^/^&]+$");//这里用正则
                        //var regExp2 = new RegExp("[^/]*&[^/^&]*$");//这里用正则匹配a&b
                        var newPath = $location.url();
                        if (bMain) {
                            newPath =$location.absUrl();
                            var regExp1 = new RegExp("#/S+");
                            newPath = newPath.replace(regExp1, '#/userPage/'+uname);
                            window.open(
                                newPath
                            );
                        } else {
                            if (newPath.match("/$")) {
                                newPath = newPath +uname;
                            } else {
                                newPath = newPath.replace(regExp, "/"+uname);
                            }
                            console.log(newPath);
                            $location.path(newPath);
                        }
                    },
                    function (data) {
                        //alert(data.msg);
                    }
                );
                //$location.path($location.url()+$rootScope.userName);


            };
            $rootScope.setStartTime = function (sTime) {
                $rootScope.dRequest.startTime = $filter('date')(sTime, "y-MM-dd");
                console.log($rootScope.dRequest);
            };
            $rootScope.setEndTime = function (sTime) {
                $rootScope.dRequest.endTime = $filter('date')(sTime, "y-MM-dd");
                console.log($rootScope.dRequest);
            };
            ////////////////////////////
            $rootScope.authority = {
                'credit': false,
                'cancelOrder': false,
                'lockUser': false
            };
            ////////////////////////////////
            // encoded parameters for exporting queries
            $rootScope.encodedParams = {};
            // open modals
            $rootScope.openModal = function (windowClass, templateUrl,size) {
                var modalInstance = $modal.open({
                        windowClass: windowClass,
                        templateUrl: templateUrl,
                        controller: 'ModalInstanceCtrl',
                        size: size
                });
           };
           $rootScope.BacktoAllData = function() {
               // console.log($state.current.name);
               $state.go($state.current.name, {}, {reload: true, inherit: false});
           };
            ///////////////////////////////
            $scope.initSide = function () {
                $scope.requestFunc = function () {
                    $scope.bLoading = true;

                    // console.log("全站：",$rootScope.dRequest);
                    // get gameTypeArray
                    // if ($rootScope.oSearch.gameTypeArray) {
                    //     console.log("reqfuesFunction - AppCtrl", $rootScope.oSearch.gameTypeArray); 
                    //     $rootScope.dRequest.gameType = $rootScope.oSearch.gameTypeArray.type; 
                    // }

                    ReportService.getStatistics($rootScope.dRequest).then(
                        function (data) {
                            // console.log(data);
                            var res = data.data;
                            $scope.dbNameList = [];
                            for(var i = 0 ; i < res.length; i++){
                                if(res[i].status =="1"){
                                    $scope.dbNameList.push(res[i]);
                                }
                            }
                            if (!$rootScope.dbNameOp ){
                                var localDb = localStorageService.get('db');
                                if (!localDb){
                                    $rootScope.dbNameOp = res[0];
                                    localStorageService.set('db', JSON.stringify($rootScope.dbNameOp));
                                }else{
                                    $rootScope.dbNameOp = JSON.parse(localDb);
                                }
                            }
                            $rootScope.User = {username :data.myInfo.username } ;
                            if (data.myInfo.level >=8 ){
                                $rootScope.authority.lockUser = true;

                            }
                            if (data.myInfo.level >=9 ){
                                $rootScope.authority.credit = true;
                                $rootScope.authority.cancelOrder = true;
                            }
                            console.log($scope.dbNameList);
                        }
                    );
                };
                $scope.requestFunc();
            };
            $scope.selectDb = function(db){
                $rootScope.dbNameOp = JSON.parse( db);
                console.log("123",$rootScope.dbNameOp);
                localStorageService.set('db',db);
                $state.reload();
            };
            $rootScope.nDeposit = 3;
            $rootScope.nWithdraw = 3;
            //$rootScope.playSound = function(){};
            $rootScope.logout = function () {
                localStorageService.remove('User');
                $state.go("login");
            };
            $rootScope.settings = {
                skin: '',
                color: {
                    themeprimary: '#2dc3e8',
                    themesecondary: '#fb6e52',
                    themethirdcolor: '#ffce55',
                    themefourthcolor: '#a0d468',
                    themefifthcolor: '#e75b8d'
                },
                rtl: false,
                fixed: {
                    navbar: false,
                    sidebar: false,
                    breadcrumbs: false,
                    header: false
                }
            };
            if (localStorageService.get('settings')){
                $rootScope.settings = localStorageService.get('settings');
            }            else{
                localStorageService.set('settings', $rootScope.settings);
            }

            $rootScope.$watch('settings', function () {
                if ($rootScope.settings.fixed.header) {
                    $rootScope.settings.fixed.navbar = true;
                    $rootScope.settings.fixed.sidebar = true;
                    $rootScope.settings.fixed.breadcrumbs = true;
                }
                if ($rootScope.settings.fixed.breadcrumbs) {
                    $rootScope.settings.fixed.navbar = true;
                    $rootScope.settings.fixed.sidebar = true;
                }
                if ($rootScope.settings.fixed.sidebar) {
                    $rootScope.settings.fixed.navbar = true;


                    //Slim Scrolling for Sidebar Menu in fix state
                    var position = $rootScope.settings.rtl ? 'right' : 'left';
                    if (!$('.page-sidebar').hasClass('menu-compact')) {
                        $('.sidebar-menu').slimscroll({
                            position: position,
                            size: '3px',
                            color: $rootScope.settings.color.themeprimary,
                            height: $(window).height() - 90,
                        });
                    }
                } else {
                    if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
                        $(".sidebar-menu").slimScroll({destroy: true});
                        $(".sidebar-menu").attr('style', '');
                    }
                }

                localStorageService.set('settings', $rootScope.settings);
            }, true);

            $rootScope.$watch('settings.rtl', function () {
                if ($state.current.name !== "persian.dashboard" && $state.current.name !== "arabic.dashboard") {
                    switchClasses("pull-right", "pull-left");
                    switchClasses("databox-right", "databox-left");
                    switchClasses("item-right", "item-left");
                }

                localStorageService.set('settings', $rootScope.settings);
            }, true);

            $rootScope.$on('$viewContentLoaded',
                function (event, toState, toParams, fromState, fromParams) {
                    if ($rootScope.settings.rtl && $state.current.name !== "persian.dashboard" && $state.current.name != "arabic.dashboard") {
                        switchClasses("pull-right", "pull-left");
                        switchClasses("databox-right", "databox-left");
                        switchClasses("item-right", "item-left");
                    }
                    if ($state.current.name === 'error404') {
                        $('body').addClass('body-404');
                    }
                    if ($state.current.name === 'error500') {
                        $('body').addClass('body-500');
                    }
                    $timeout(function () {
                        if ($rootScope.settings.fixed.sidebar) {
                            //Slim Scrolling for Sidebar Menu in fix state
                            var position = $rootScope.settings.rtl ? 'right' : 'left';
                            if (!$('.page-sidebar').hasClass('menu-compact')) {
                                $('.sidebar-menu').slimscroll({
                                    position: position,
                                    size: '3px',
                                    color: $rootScope.settings.color.themeprimary,
                                    height: $(window).height() - 90,
                                });
                            }
                        } else {
                            if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
                                $(".sidebar-menu").slimScroll({destroy: true});
                                $(".sidebar-menu").attr('style', '');
                            }
                        }
                    }, 500);

                    window.scrollTo(0, 0);
                });
        }
    ]);