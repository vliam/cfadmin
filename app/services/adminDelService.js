angular.module('app')

    .factory('AdminDelService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
            function ($http, $q, ConfigService, localStorageService, AuthService) {
                var adminDelService = {};


                ///删除用户信息
                adminDelService.DelAdmin = function (dRequest) {

                    var User = AuthService.getCurrentUser();
                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: ConfigService.getHost()
                        +'/admin/Userinfo.action;jsessionid='+ User.token + '?do=deleteUser&wid=t1',
                        params: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                console.log(data);
                                if (data.code != 1) {
                                    deferred.reject(data);
                                } else {

                                    deferred.resolve(data);
                                }
                            },
                            function (data) {
                                console.log("user list error");
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };

                return adminDelService;
            }]);

