/**
 * 服务
 */
angular.module('app')

    .factory('AdminDetailService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService','md5',
            function ($http, $q, ConfigService, localStorageService, AuthService,md5) {
                var adminDetailService = {};

                //获取添加类型
                adminDetailService.getType = function (dRequest) {

                    var User = AuthService.getCurrentUser();
                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: ConfigService.getHost()
                        +'/admin/Userinfo.action;jsessionid='+ User.token + '?do=getUserType&wid=t1',
                        params: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;

                                if (data.code != 1) {
                                    deferred.reject(data);
                                } else {
                                    console.log(data);
                                    deferred.resolve(data);
                                }
                            },
                            function (data) {
                                console.log("user list error");
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };

                //获取添加的admin的用户信息
                adminDetailService.addUser = function (dRequest) {

                    var User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    dRequest.password = md5.createHash(dRequest.password );
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost()
                        +'/admin/Userinfo.action;jsessionid='+ User.token + '?do=addUser&wid=t1',
                        params: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                console.log(data);
                                if (data != 1) {
                                    deferred.reject(data);
                                } else {

                                    deferred.resolve(data);
                                }
                            },
                            function (data) {
                                console.log("user list error");
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };

                return adminDetailService;
            }]);
