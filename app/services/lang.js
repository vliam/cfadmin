angular.module('app')
    .factory('T', ['$translate', function($translate) {
    console.log('语言');
    var T = {
        T:function(key) {
            if(key){
                return $translate.instant(key);
            }
            return key;
        },
        txt:function(key){
            return T.T(key);
        },
        getI18n: function(sCtrlName){
            if(sCtrlName =="ReportCtrl"){// report ctrl
                return{
                    user : T.T('User'),
                    betting : T.T("Betting"),
                    effectBet : T.T("Valid Bet"),
                    bonus : T.T("Bonus"),
                    deposit : T.T("Deposit"),
                    withdraw : T.T("Withdraw"),
                    rebate : T.T("Rebate"),
                    eventGift : T.T("Gift"),
                    agent : T.T("Agent"),
                    website : T.T("Website"),
                    bet : T.T("Betting"),
                    profit : T.T("Profit"),
                    currency: T.T("CURRENCY")
                }
            }else if(sCtrlName =="AccountCtrl"){
                 return {
                     CreateTime : T.T('CreateTime'),
                     orderId :T.T("Order Id"),
                     user : T.T('User'),

                     withdrawAmount : T.T('WithdrawAmount'),
                     state : T.T('State'),

                     rechargeAmount : T.T('RechargeAmount'),
                     description : T.T('Description'),

                     beforeAmount :  T.T('BeforeAmount'),
                     afterAmount :  T.T('AfterAmount')
                 }
            }else if(sCtrlName == "UserCtrl") {
                return {
                    Rtime : T.T('Reg Time'),
                    user : T.T('User'),
                    balance : T.T('Balance'),
                    rebate : T.T('Rebate'),
                    login : T.T('Login'),
                    status : T.T("Status")
                }
            }else if(sCtrlName == "OrderCtrl"){
                return {
                    time : T.T('Time'),
                    raceNum : T.T('RaceNum'),
                    user : T.T('User'),
                    amount : T.T("Amount"),
                    winAmount : T.T('WinAmount'),
                    arena : T.T('Arena'),
                    orderNum : T.T("Order Id"),
                    state : T.T('State'),
                    odds : T.T('Odds'),
                    orderCancel:T.T('Cancel order'),
                    confirmDelete:T.T('confirm delete')
                }
            }else if (sCtrlName == "HistoryCtrl"){
                return {
                    time: T.T('Time'),
                    issue : T.T('Issue'),
                    arena: T.T('Arena'),
                    state: T.T('State'),
                    results: T.T('Results'),
                    modify: T.T('Modify'),
                    initial : T.T('initial'),
                    inStock : T.T('Betting'),
                    notEntered : T.T('result pending'),
                    inTheAward : T.T('inTheAward'),
                    beenAwarded : T.T('beenAwarded'),
                    allGames  :  T.T('All Games'),
                    W : T.T('W'),
                    M : T.T('M'),
                    C : T.T('C'),
                    D : T.T('D'),
                    confirm :T.T('confirm')
                }
            }else if(sCtrlName == "LoginCtrl"){
                return {
                    login : T.T("Login")
                }
            }else if (sCtrlName == "NavbarCtrl"){
                return {
                    Logout : T.T("Logout")
                }
            }else if (sCtrlName == "SearchbarCtrl"){
                return {
                    User : T.T('User'),
                    betAmountLL  :  T.T('betAmountLL'),
                    betAmountCap  :  T.T('betAmountCap'),
                    minAmount  :  T.T('Min Amount'),
                    maxAmount  :  T.T('Max Amount'),
                    lotteryType  :  T.T('lotteryType'),
                    gameType:T.T('gameType'),
                    allChannels  :  T.T('allChannels'),
                    allStatus  :  T.T('allStatus'),
                    allGames  :  T.T('All Games'),
                    agent  :  T.T("Agent"),
                    loginIP  :  T.T('loginIP'),
                    orderNumber  :  T.T("Order Id"),
                    issue  :  T.T('issue'),
                    minBonus  :  T.T('Min Bonus'),
                    maxBonus  :  T.T('Max Bonus'),
                    allRebate  :  T.T('allRebate'),
                    allType  :  T.T('allType'),
                    regTimeSort  :  T.T('regTimeSort'),
                    accountBalanceSort  :  T.T('accountBalanceSort'),
                    txt: T.T
                }
            }else if(sCtrlName == "SidebarCtrl"){
                return {
                    home : T.T("home"),
                    dataStatistics : T.T("DataStatistics"),
                    dayKnot : T.T("DayKnot"),
                    userDayKnot : T.T("UserDayKnot"),
                    agentDayKnot : T.T("AgentDayKnot"),
                    camera : T.T("LIVE_CAMERA"),

                    memberMangement : T.T("MemberMangement"),
                    memberList : T.T("MemberList"),

                    bettingRecord_title : T.T("BettingRecord_title"),
                    winningOrder : T.T("WinningOrder"),
                    cancelOrder : T.T("CancelOrder"),
                    bettingRecord : T.T("BettingRecord"),
                    matchResults : T.T("MatchResults"),

                    financialManagement : T.T("FinancialManagement"),
                    businessChanges : T.T("BusinessChanges"),
                    rechargeRecord : T.T("RechargeRecord"),
                    withdrawalsRecord : T.T("WithdrawalsRecord")
                }
            }else if(sCtrlName == "AdminCtrl"){
                    return {
                        addAdmin: T.T('add admin'),
                        createTime : T.T('CreateTime'),
                        user : T.T('User'),
                        status : T.T('Status'),
                        type : T.T('Type'),
                        normal :T.T('normal'),
                        freeze : T.T('freeze'),
                        unavailable : T.T('unavailable'),
                        modify : T.T('modify')
                    }
            }else if(sCtrlName == "AdminDetailCtrl"){
                return {
                    createTime : T.T('CreateTime'),
                    userName : T.T('UserName'),
                    password : T.T('Password'),
                    paypasswd : T.T('Paypasswd'),
                    type : T.T('Type'),
                    clear : T.T('Clear'),
                    save : T.T('Save'),
                    allType : T.T('allType')
                }
            }else if (sCtrlName == "AdminUpdateCtrl"){
                return {
                    createTime : T.T('CreateTime'),
                    userName : T.T('UserName'),
                    password : T.T('Password'),
                    paypasswd : T.T('Paypasswd'),
                    superior : T.T('Superior'),
                    type : T.T('Type'),
                    clear : T.T('Clear'),
                    save : T.T('Save'),
                    allType : T.T('allType'),
                    delete : T.T('Delete'),
                    changepassword : T.T('change password')
                }
            };
        return {};
        }

    };
    return T;
}]);