/**
 * 服务
 */
angular.module('app')
//日结报表服务
    .factory('ReportService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService', "$state",
            function ($http, $q, ConfigService, localStorageService, AuthService, $state) {
                var reportService = {};
                var User;
                //得到库名列表
                reportService.getStatistics  = function (dRequest) {
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/sys/getDBNameAndId.do;jsessionid='
                        + User.token ,
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.code) {//有数据
                                        if(data.code == '0003'){
                                            console.log('0003');
                                            AuthService.relogin();
                                            deferred.reject(data);
                                        }else{
                                            deferred.resolve(data);
                                        }
                                    }
                                 else {
                                    AuthService.relogin();
                                    deferred.reject(data);
                                }
                            }

                        );
                    return deferred.promise;
                },

                reportService.getReportView = function (dRequest) {
                    dRequest.dbid = AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/view/getReportView.do;jsessionid='
                        + User.token,
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;

                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.code) {//有数据
                                    deferred.resolve(data);
                                } else if (data.split("|")[0] === "0") {
                                    console.log(data);
                                    AuthService.relogin();
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                /////////////////////////////////////////////
                reportService.findUserDailyReportList = function (dRequest) {
                    dRequest.dbid = AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/UserDailyReport.action;jsessionid='
                        + User.token + '?do=findUserDailyReportList',
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                if (data == null) {
                                    deferred.reject("ERR_NETWORK");
                                } else if (data.code) {//有数据
                                    if(data.code == '0003'){
                                        console.log('0003');
                                        AuthService.relogin();
                                        deferred.reject(data);
                                    }else{
                                        deferred.resolve(data);
                                    }
                                }  else {
                                    AuthService.relogin();
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                
                //////Credit Service
                reportService.creditService = function (dRequest) {
                    dRequest.wid = AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/merchant/updateMerchantQuota.do;jsessionid='
                        + User.token,
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                if (data == null) {
                                    deferred.reject("ERR_NETWORK");
                                } else if (data.code) {//有数据
                                    if(data.code == '0003'){
                                        console.log('0003');
                                        AuthService.relogin();
                                        deferred.reject(data);
                                    }else{
                                        deferred.resolve(data);
                                    }
                                }  else {
                                    AuthService.relogin();
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                ///////////////
                
                reportService.findAllAgentDailyList = function (dRequest) {
                    dRequest.dbid = AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/UserDailyReport.action;jsessionid='
                        + User.token + '?do=findAllAgentDailyList',
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.code) {//有数据
                                    deferred.resolve(data);
                                } else if (data.split("|")[0] === "0") {
                                    console.log(data);
                                    AuthService.relogin();
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                reportService.findAllUserDailyList = function (dRequest) {
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/UserDailyReport.action;jsessionid='
                        + User.token + '?do=findAllUserDailyList',
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                }else if (data.code) {//有数据
                                    if(data.code == '0003'){
                                        console.log('0003');
                                        AuthService.relogin();
                                        deferred.reject(data);
                                    }else{
                                        deferred.resolve(data);
                                    }
                                }  else {
                                    AuthService.relogin();
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };

                // export User Daily
                reportService.exportUserDaily = function(dRequest) {
                    // gonna uncomment this for the actual implementation.
                    // this is for just testing of the data only.
                    dRequest.wid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    console.groupCollapsed("export Order Service Received Data");
                    console.info(dRequest);
                    console.groupEnd();

                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/export/ExportUserDailyReportList.do;jsessionid='
                        + User.token + '',
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                    .then(
                        function (resp, status, header, config) {
                            data = resp.data;
                            console.group("export Order ServiceResponse from Server");
                            console.log(resp);
                            console.groupEnd();

                            if(data && data.code == 1) {
                                var res = JSON.parse(data.data);
                                console.groupCollapsed("Export Order: Object Data");
                                console.log(res);
                                console.groupEnd();
                                deferred.resolve(res);
                            } else if(!data || data.code === "0098") { // exceeds the total number of accepted queries
                                deferred.resolve(data.code+"^"+data.itemTotal);
                                console.log("Too much data, please narrow the scope of the query");
                            } else if(!data || data.code === "0002") { // if no data returned from the server to service
                                deferred.resolve(data.code+"^"+data.description);
                                console.log("no data available");
                            } else if (!data || data.code == "0003"){
                                deferred.resolve(data);
                                console.log('order err');
                                AuthService.relogin();
                            } else if(data.split(",").length > 0) {
                                console.log("passed: it is a csv format");
                                deferred.resolve(data);
                            } else {
                                
                                deferred.reject(data);
                                
                            }
                        },
                        function (data) {
                            AuthService.relogin();
                            deferred.reject(data);
                        }
                                
                    );
                    return deferred.promise;
                };


                return reportService;
            }])
    //用户service
    .factory('UserService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
            function ($http, $q, ConfigService, localStorageService, AuthService) {
                var userService = {};
                var User;
                //这里返回用户列表
                userService.getList = function (dRequest) {
                    dRequest.dbid = AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost()
                        + '/view/getAdminList.do;jsessionid='
                        + User.token,
                        // params: dRequest,
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                console.log(data);
                             if (data == null) {
                                    deferred.reject("ERR_NETWORK");
                                } else if (data.code == '0003'){
                                 AuthService.relogin();
                                 deferred.reject(data);
                             }else {
                                     data = data.data;
                                     if (data.pageId) {//有数据
                                         deferred.resolve(data);
                                     } else{
                                         deferred.reject(data)
                                     }
                                 }
                            },
                            function (data) {
                                console.log("user list error");
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                //139.162.10.174:8158/manages/UserinfoServlet?do=illegalList
                //非法请求
                userService.getIllegalList = function (dRequest) {
                    dRequest.dbid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                        + User.token + '?do=illegalList&json=true',
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.pageTotal) {//有数据
                                    deferred.resolve(data);
                                } else
                                    deferred.reject(data);
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                //http://139.162.10.174:8158/manages/UserinfoServlet?do=loginLog
                //用户IP列表
                userService.getLoginLog = function (dRequest) {
                    dRequest.dbid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                        + User.token + '?do=loginLog&json=true',
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.pageTotal) {//有数据
                                    deferred.resolve(data);
                                } else
                                    deferred.reject(data);
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                //user detail
                //get detail by id
                userService.getDetail = function (dRequest) {
                    dRequest.dbid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    var sDo = "getMemberByUserId";

                    if (!dRequest.id || dRequest.id == "") {
                        sDo = "getMemberByUserName";
                    }
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/manages/UserinfoServlet;jsessionid='
                        + User.token + '?do=' + sDo + '&json=true',
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                console.log("qwer",data);
                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.member) {//有数据
                                    deferred.resolve(data);
                                } else
                                    deferred.reject(data);
                            },
                            function (data) {
                                deferred.reject("网络出错，请重试");
                            }
                        );
                    return deferred.promise;
                };
                
                /////////////Lock User
                userService.lockUser = function (dRequest) {
                    dRequest.userWid = AuthService.getWid(); //get the userwid
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/admin/Userinfo.action;jsessionid='
                        + User.token,
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                //console.log("datadata",data);
                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.code == 1) {//success
                                    deferred.resolve(data);
                                } else{
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                //////////////////////

                /////////////////////   // export User Lists
                // export User Lists
                userService.exportUserLists = function(dRequest) {
                    dRequest.wid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    console.groupCollapsed("export User Lists Service Received Data");
                    console.info(dRequest);
                    console.groupEnd();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/export/ExportAdminList.do;jsessionid='
                        + User.token + '',
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                    .then(
                        function (resp, status, header, config) {
                            data = resp.data;
                            console.group("export User Lists ServiceResponse from Server");
                            console.log(resp);
                            console.groupEnd();

                            if(data && data.code == 1) {
                                var res = JSON.parse(data.data);
                                console.groupCollapsed("Export UserLists: Object Data");
                                console.log(res);
                                console.groupEnd();
                                deferred.resolve(res);
                            } else if(!data || data.code === "0098") { // exceeds the total number of accepted queries
                                deferred.resolve(data.code+"^"+data.itemTotal);
                                console.log("Too much data, please narrow the scope of the query");
                            } else if(!data || data.code === "0002") { // if no data returned from the server to service
                                deferred.resolve(data.code+"^"+data.description);
                                console.log("no data available");
                            } else if (!data || data.code == "0003"){
                                deferred.resolve(data);
                                console.log('order err');
                                AuthService.relogin();
                            } else if(data.split(",").length > 0) {
                                console.log("passed: it is a csv format");
                                deferred.resolve(data);
                            } else {
                                
                                deferred.reject(data);
                                
                            }
                        },
                        function (data) {
                            AuthService.relogin();
                            deferred.reject(data);
                        }
                                
                    );
                    return deferred.promise;
                };
                /////////////////////
                return userService;
            }])
    //订单service
    .factory('OrderService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
            function ($http, $q, ConfigService, localStorageService, AuthService) {
                var orderService = {};
                var User;
                //这里返回列表
                orderService.getList = function (dRequest) {
                    dRequest.dbid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        //DoBusiness?do=orderList
                        url: ConfigService.getHost() + '/cp/pages/OrderQueryCenter.action;jsessionid=' + User.token + '?do=queryOrder',
                        // params: dRequest,
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                    console.log(data);

                                    if(data && data.code == 1){
                                        var res = JSON.parse(data.data);
                                        console.log(res);
                                        deferred.resolve(res);

                                    }else if (!data || data.code == "0003"){
                                        deferred.resolve(data);
                                        console.log('order err');
                                        AuthService.relogin();

                                    }else {

                                        deferred.reject(data);

                                    }
                                },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }

                        );
                    return deferred.promise;
                };
                orderService.getDetail = function (dRequest) {
                    dRequest.dbid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/manages/OrderQueryCenter.action;jsessionid='
                        + User.token + '?do=orderDetail&json=true&orderjson=true',
                        // params: dRequest,
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;

                                alert(typeof data);
                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.id) {//有数据
                                    deferred.resolve(data);
                                } else
                                    deferred.reject(data);
                            },
                            function (data) {
                                //AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };

                //提交删除订单
                orderService.rollBackOrder = function (dRequest) {
                    dRequest.wid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/cp/pages/OrderQueryCenter.action;jsessionid='
                        + User.token + '?do=cancelOrder',
                        // params: dRequest,
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;

                                if (data == null) {
                                    deferred.reject("网络出错，请重试");
                                } else if (data.code == 1) {//有数据
                                    deferred.resolve(data);
                                } else{
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                //AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                
                // export Order
                orderService.exportOrder = function(dRequest) {
                    // gonna uncomment this for the actual implementation.
                    // this is for just testing of the data only.
                    dRequest.wid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    console.groupCollapsed("export Order Service Received Data");
                    console.info(dRequest);
                    console.groupEnd();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/export/ExportOrder.do;jsessionid='
                        + User.token + '',
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                    .then(
                        function (resp, status, header, config) {
                            data = resp.data;
                            console.group("export Order ServiceResponse from Server");
                            console.log(resp);
                            console.groupEnd();

                            // this data is for service testing for the return value of data from the server
                            /*data = {
                                itemTotal: 812434, 
                                code: "0098", 
                                description: "Too much data, please narrow the scope of the query!"
                            };
                            data = {
                                code: "0002", 
                                description: "no data!"
                            };*/

                            if(data && data.code == 1) {
                                var res = JSON.parse(data.data);
                                console.groupCollapsed("Export Order: Object Data");
                                console.log(res);
                                console.groupEnd();
                                deferred.resolve(res);
                            } else if(!data || data.code === "0098") { // exceeds the total number of accepted queries
                                deferred.resolve(data.code+"^"+data.itemTotal);
                                console.log("Too much data, please narrow the scope of the query");
                            } else if(!data || data.code === "0002") { // if no data returned from the server to service
                                deferred.resolve(data.code+"^"+data.description);
                                console.log("no data available");
                            } else if (!data || data.code == "0003"){
                                deferred.resolve(data);
                                console.log('order err');
                                AuthService.relogin();
                            } else if(data.split(",").length > 0) {
                                console.log("passed: it is a csv format");
                                deferred.resolve(data);
                            } else {
                                
                                deferred.reject(data);
                                
                            }
                        },
                        function (data) {
                            AuthService.relogin();
                            deferred.reject(data);
                        }
                                
                    );
                    return deferred.promise;
                };

                return orderService;
            }])

    //财务service
    .factory('AccountDetailService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
            function ($http, $q, ConfigService, localStorageService, AuthService) {
                var accountDetailService = {};
                var User;
                /*
                 page=页码
                 可选参数
                 pageSize=每页显示数量 默认为50
                 userName=用户名
                 startTime=开始时间 YYYY-mm-dd
                 endTime=结束时间 YYYY-mm-dd
                 type=类型
                 　　(101=第三支付
                 　　102=银行转账
                 　　103=投注返点
                 　　104=他人赠与
                 　　105=返奖
                 　　106=注册送28
                 　　
                 　　201=投注扣款
                 　　202=提现
                 　　204=转账
                 　　205=退奖)
                 */
                accountDetailService.getList = function (dRequest) {
                    dRequest.wid =  AuthService.getWid();
                    //这里返回某种订单的列表
                    User = AuthService.getCurrentUser();
                    console.log(User);
                    var sUrl = ConfigService.getHost() +
                        '/view/AccountDetailList.do' +
                        ';jsessionid=' + User.token;
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: sUrl,
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (dataRecv, status, header, config) {
                                data = dataRecv.data;
                                if (data == null) {
                                    console.log("empty");
                                    deferred.reject("网络出错，请重试");
                                } else if (data.pageId) {//有数据
                                    data.page = data.pageId;
                                    deferred.resolve(data);
                                } else {
                                    console.log(data);
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                //////////////////////////////////////////////////
                accountDetailService.getBusiness = function (dRequest) {
                    dRequest.wid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    console.log('交易变动-');
                    console.log(User);
                    var sUrl = ConfigService.getHost() +
                        '/AccountDetailServlet;jsessionid=' + User.token + '?do=list';
                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: sUrl,
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (dataRecv, status, header, config) {
                                data = dataRecv.data;
                                if (data == null) {
                                    console.log("empty");
                                    deferred.reject("网络出错，请重试");
                                } else if (data.pageId) {//有数据
                                    // console.log(data.pageContent);
                                    data.page = data.pageId;

                                    deferred.resolve(data);
                                } else {
                                    console.log(data);
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                accountDetailService.getDeposit = function (dRequest) {
                    dRequest.wid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    console.log('充值-');
                    console.log(User);
                    var sUrl = ConfigService.getHost() +
                        '/AccountDetailServlet;jsessionid=' + User.token + '?do=list';
                    var deferred = $q.defer();


                    $http({
                        method: 'POST',
                        url: sUrl,
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (dataRecv, status, header, config) {
                                data = dataRecv.data;
                                if (data == null) {
                                    console.log("empty");
                                    deferred.reject("网络出错，请重试");
                                } else if (data.pageId) {//有数据
                                    // console.log(data.pageContent);
                                    deferred.resolve(data);
                                } else {
                                    console.log(data);
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                console.log('zheshi sm dd');
                                console.log(data);
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                accountDetailService.getWithdraw = function (dRequest) {
                    dRequest.wid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    console.log('提现-');
                    console.log(User);
                    var sUrl = ConfigService.getHost() +
                        '/AccountDetailServlet;jsessionid=' + User.token + '?do=list';

                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: sUrl,
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (dataRecv, status, header, config) {
                                data = dataRecv.data;
                                if (data == null) {
                                    console.log("empty");
                                    deferred.reject("网络出错，请重试");
                                } else if (data.pageId) {//有数据
                                    // console.log(data.pageContent);

                                    deferred.resolve(data);
                                } else {
                                    console.log(data);
                                    deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                // export Account Details
                accountDetailService.exportAccountDetails = function(dRequest) {
                    dRequest.wid =  AuthService.getWid();
                    User = AuthService.getCurrentUser();
                    console.groupCollapsed("export Account Details Service Received Data");
                    console.info(dRequest);
                    console.groupEnd();
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/export/ExportAccountDetailList.do;jsessionid='
                        + User.token + '',
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                    .then(
                        function (resp, status, header, config) {
                            data = resp.data;
                            console.group("export Acccount Details ServiceResponse from Server");
                            console.log(resp);
                            console.groupEnd();

                            if(data && data.code == 1) {
                                var res = JSON.parse(data.data);
                                console.groupCollapsed("Export Account Details: Object Data");
                                console.log(res);
                                console.groupEnd();
                                deferred.resolve(res);
                            } else if(!data || data.code === "0098") { // exceeds the total number of accepted queries
                                deferred.resolve(data.code+"^"+data.description+"^"+data.itemTotal);
                                console.log("Too much data, please narrow the scope of the query");
                            } else if(!data || data.code === "0002") { // if no data returned from the server to service
                                deferred.resolve(data.code+"^"+data.description);
                                console.log("no data available");
                            } else if (!data || data.code == "0003"){
                                deferred.resolve(data);
                                console.log('order err');
                                AuthService.relogin();
                            } else if(data.split(",").length > 0) {
                                console.log("passed: it is a csv format");
                                deferred.resolve(data);
                            } else {
                                
                                deferred.reject(data);
                                
                            }
                        },
                        function (data) {
                            AuthService.relogin();
                            deferred.reject(data);
                        }
                                
                    );
                    return deferred.promise;
                };
                return accountDetailService;
    }])
    .factory('ModalInstanceExport',
        ['$rootScope', '$window', 'T',
            function ($rootScope, $window, T) {
                var oi18n = {"txt": T.T};
                var exportObj = {};
                exportObj.doExport = function(eParams, data) {
                    var errorCode = function(insertParams = "") {
                        return {
                            "0002" : oi18n.txt("No Data Found!"),
                            "0098" : [
                                        oi18n.txt("Please narrow the scope of your request"),
                                        oi18n.txt("Your actual query lines:") + " <b>"+insertParams.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</b>",
                                        oi18n.txt("The query must be around") + " <b>30,000</b> " + oi18n.txt("lines")
                                    ]
                        }
                    };
                    if(data.split("^").length < 2) {
                        // downloading data as csv file format
                        // create invisible anchor tag to download encoded csv data
                        var anchorTag = document.createElement("a"), blob = new Blob([data], {type: "octet/stream"}), url = $window.URL || $window.webkitURL, d = new Date();
                        anchorTag.style = "display: none";
                        anchorTag.href = url.createObjectURL(blob);
                        anchorTag.download = ((eParams === "order") ? oi18n.txt("Order") 
                        : 
                        ((eParams === "account") ? oi18n.txt("AccountDetails") 
                        : 
                        ((eParams === "userdaily") ? oi18n.txt("UserDaily") 
                        : oi18n.txt("UserLists") ))) + "_" + 
                        d.getFullYear()+""+(d.getMonth()+1)+""+d.getDate()+""+d.getHours()+""+d.getMinutes()+""+d.getSeconds()+".csv";
                        anchorTag.click();
                        window.URL.revokeObjectURL(url.createObjectURL(blob));
                    } else {
                        var eCode = data.split("^")[0], description = data.split("^")[1];
                        $rootScope.ExportDataMessage = (eCode === "0002") ? 
                        "<center><p>"+errorCode()[eCode]+"</p></center>"
                        : 
                        "<p>"+errorCode(description)[eCode][0]+
                        "<br>"+errorCode(description)[eCode][1]+
                        "<br>"+errorCode(description)[eCode][2]+"</p>";
                        $rootScope.openModal('modal-message modal-danger','DangerModal.html');
                    }
                };

                exportObj.addInput = function() {
                    $rootScope.encodedParams = {};
                    //$rootScope.dRequest.username = ($rootScope.dRequest.userName) ? $rootScope.dRequest.userName : "";
                    // pageSize
                    console.groupCollapsed("Inside addInput");
                    console.info($rootScope.dRequest);
                    console.log($rootScope.dRequest.orderBy);
                    console.groupEnd();
                    if($rootScope.dRequest.hasOwnProperty("userName")) { 
                        $rootScope.encodedParams.username = $rootScope.dRequest.userName; 
                    }
                    //console.info($rootScope.encodedParams);
                    for(key in $rootScope.dRequest) {
                        if((key === "endTime") || (key === "startTime") || (key === "lotteryCode") || (key === "orderNo") || (key === "username") || (key === "userName") 
                        || (key === "status") || (key === "type") || (key === "orderBy") || (key === "upusername")) {    // OrderCtrl, AccountDetails, UserDaily
                            //if($rootScope.dRequest[key]) {
                            console.log(key+"='"+$rootScope.dRequest[key]+"'");
                            console.log("TYPE:"+ typeof $rootScope.dRequest[key]);
                            $rootScope.encodedParams[key] = $rootScope.dRequest[key];
                        }
                    }
                };
                return exportObj;
            }
        ]
    )
;


// $state.current.name