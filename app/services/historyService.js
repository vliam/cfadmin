/**
 * 服务
 */
angular.module('app')
//赛事结果service这里只涉及到主库
    .factory('HistoryService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
            function ($http, $q, ConfigService, localStorageService, AuthService) {
                var historyService = {};
                var User;
                //getHistory
                historyService.getHistory = function (dRequest) {
                    User = AuthService.getCurrentUser();
                    dRequest.dbid='t1';
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/info/getHistoryList.do;jsessionid='
                        + User.token,
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                if (!data.code && !data.itemTotal) {
                                    AuthService.relogin();
                                } else {
                                    console.log(data);
                                    if (data == null) {
                                        deferred.reject("网络出错，请重试");
                                    } else if (data.itemTotal) {//有数据
                                        deferred.resolve(data);
                                    } else
                                        deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                return historyService;
            }])

    .factory('UpdateBonusNumberService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
            function ($http, $q, ConfigService, localStorageService, AuthService) {
                var updateBonusNumberService = {};
                var User;
                updateBonusNumberService.getBounusInfo = function (dRequest) {
                    User = AuthService.getCurrentUser();
                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: ConfigService.getHost() + '/cp/MainIssue.action;jsessionid='
                        + User.token +'?do=RollBack',
                        params: dRequest,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    })
                        .then(
                            function (resp, status, header, config) {

                                data = resp.data;
                                if (!data) {
                                    AuthService.relogin();
                                } else {
                                    console.log(data.code);
                                    if (data.code == null) {
                                        deferred.reject("网络出错，请重试");
                                    } else if (data.code ) {//有数据
                                        deferred.resolve(data);
                                    } else
                                        deferred.reject(data);
                                }
                            },
                            function (data) {
                                AuthService.relogin();
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                return updateBonusNumberService;
            }])
;