/**
 * 服务
 */
angular.module('app')
      //admin service
    .factory('AdminService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
            function ($http, $q, ConfigService, localStorageService, AuthService) {
                var adminService = {};

                //这里返回admin列表
                adminService.getList = function (dRequest) {
                    var User;

                    // dRequest.wid =  't1';
                    User = AuthService.getCurrentUser();
                    // dRequest.userName = User.username;

                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: ConfigService.getHost()
                            +'/admin/Userinfo.action;jsessionid='+ User.token + '?do=getAdminList&wid=t1',
                        data: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                var data = resp.data;
                             if (data == null) {
                                    deferred.reject("ERR_NETWORK");
                                } else if (data.code == '0003'){
                                 AuthService.relogin();
                                 deferred.reject(data);
                             }else {
                                     if (data.data) {//有数据
                                         deferred.resolve(data);
                                     } else{
                                         deferred.reject(data)
                                     }
                                 }
                            },
                            function (data) {
                                console.log("user list error");
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                //
                return adminService;
            }]);
