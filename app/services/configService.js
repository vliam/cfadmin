/**
 * 这里放一些常量，和字典
 */
angular.module('app')
    .service('ConfigService',  ['$location', function ($location) {
        var mainUrl = "http://139.162.35.213:8083/bomb";
        var testUrl = "http://139.162.23.175:8083/bomb";
        var mainWid = "t1";
        var s = {
            isProd:function(){
                var str = $location.absUrl();
                str = str.substr(0, 30);
                // console.log(str);
                if (str.indexOf("test") > -1  ||str.indexOf("127.0") > -1 ||str.indexOf("localhost") > -1 ) {
                    return false;
                 }else {
                    return true;

                }
            },
            getHost: function () {
                if (!this.isProd()) {
                    return testUrl;
                    //return mainUrl; // for testing uncomment the line of code above after testing
                }else{
                    return mainUrl;
                }
            },
            getWid: function () {
                {
                    return mainWid;
                }
            },
            getDateTime: function (){
                var d = new Date();
                var sTime = d.toTimeString();
                var month = d.getMonth()+1;
                var date = d.getDate();
                var sDate = d.getFullYear() +"-"
                    + (month >=10? '':'0')+ month + "-"
                    + (date >=10 ? '':'0') +date;
                return sDate +' '+sTime.substr(0,8);
            }
        };
        return s;
    }])
    /*  配置搜索栏 决定了某个页面上面search栏的组合 */
    .service('ConstService', [function () {
        var oSearchBarConfig = {

            "dashboard":{
                startTime: 1,
                isGameType:1, // 是否有[游戏]的下拉框
            },
            "report": {
                  "startTime": 1,
                  isGameType:1, // 是否有[游戏]的下拉框
            },
            "date": {
                "startTime": 1,
                "hideUserName":1,
                "isGameType":1, // 是否有[游戏]的下拉框
            },
            "orderList": {
                "orderNo":1,
                "autoOrderNo":1,
                "issue": 1,
                "lotteryCode": 1,
                "status": 1,    // 是否有[中奖状态]的下拉框
                "isGameType":1, // 是否有[游戏]的下拉框
                "betAA": 1,
                "bonAA": 1,
                "startTime": 1
            },
            "userList": {
                "realname": 0, "upusername": 0, "status": 1,
                "type": 1, "orderBy": 1, "registerTime": 1, "rebate_info": false, "mobile":false,
                "startTime":true
            },
            "adminList": {
            },
            'businessList': {
                'startAmount':true, 'startTime':true
            },
            'depositList': {
                'startAmount':true, 'startTime':true
            },
            'withdrawList': {
                'startAmount':true, 'startTime':true
            },
            'OQuery': {
                "betAA":true, "bonAA":true, "autoOrderId":true,
                "startTime":true,"orderNo":true,"lotteryCode":true,"issue":true,
                "status" : true, "type":true
            },

            'bankList': {
                "startTime":true, "mobile":true
            },
            'issueList': {
                "lotteryCode":true,"issue":true, "status" : true
            },//期次

            'loginLog': {
                "ip":true, "startTime":true
            },
            'illegal':{
                "startTime":true
            },
            'history': {
               "hideUserName":true,
                "lotteryCode":true,
                "startTime":true,
                "endTime":true,
                "issue":true,
                "status":true,
                "isGameType":1, // 是否有[游戏]的下拉框
            }

        };
        var constService = {
            getUserStatus: function (iStatus) {
                var userStatus = ["", "<font color='blue'></font>", "<font color='red'>invalid</font>", "<font color='red'>deleted</font>"];
                return userStatus[iStatus];
            },
            getRegFrom: function (i) {
                var aRegFrom = ["连接注册", "其他1", "代理开通", ""];
                return aRegFrom[i];
            },
            //传入page的名字，返回搜索bar的配置（即可用的参数，可以冗余）
            getSearchBar: function (sPage) {
                return oSearchBarConfig[sPage];
            }
        };
        return constService;
    }])
    .service('dateService', [function () {
        var dateService = {};
        dateService.stringMonth = function (i) {
            var d = ( new Date()).valueOf();
            var date = new Date(d);
            while (i > 0) {
                date = new Date(d);
                var tmp = new Date(date.getFullYear(), date.getMonth(), 0);
                d -= tmp.getDate() * 3600 * 24 * 1000;
                i--;
            }
            date = new Date(d);
            var Y = date.getFullYear() + '-';
            var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            return (Y + M );
        };
        dateService.stringDate = function (i) {
            var t = (new Date()).valueOf() - i * 3600 * 24 * 1000;
            var date = new Date(t);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()  );
            return (Y + M + D);
        };
        dateService.stringTime = function (i) {
            var t = (new Date()).valueOf() - i * 3600 * 24 * 1000;
            var date = new Date(t);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()  ) + ' ';
            hms = '23:59:59';
            return (Y + M + D + hms);
        };
        dateService.getDateArray = function (n) {
            var arrDate = new Array(n);
            arrDate[0] = '今天';
            arrDate[1] = '昨天';
            for (i = 2; i < n; i++) {
                var t = (new Date()).valueOf() - i * 3600 * 24 * 1000;
                var date = new Date(t);
                M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '月';
                D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()  ) + '日';
                arrDate[i] = M + D;
            }
            return arrDate;
        };
        dateService.stringCurrTime = function () {
            var date = new Date();
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()  ) + ' ';
            hms = date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
            return (Y + M + D + hms);
        };

        return dateService;
        }])
        //这里设置httpProvider,重写transformRequest，使用data
        .config(['$httpProvider', function ($httpProvider) {
            // Intercept POST requests, convert to standard form encoding
            $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.transformRequest.unshift(function (data, headersGetter) {
                var key, result = [];

                if (typeof data === "string")
                    return data;

                for (key in data) {
                    if (data.hasOwnProperty(key))
                        result.push(encodeURIComponent(key) + "=" + encodeURIComponent(data[key]));
                }
                return result.join("&");
            });
            $httpProvider.defaults.transformRequest = [function (data) {
                /**
                 * The workhorse; converts an object to x-www-form-urlencoded serialization.
                 * @param {Object} obj
                 * @return {String}
                 */
                var param = function (obj) {
                    var query = '';
                    var name, value, fullSubName, subName, subValue, innerObj, i;

                    for (name in obj) {
                        value = obj[name];

                        if (value instanceof Array) {
                            for (i = 0; i < value.length; ++i) {
                                subValue = value[i];
                                fullSubName = name + '[' + i + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        } else if (value instanceof Object) {
                            for (subName in value) {
                                subValue = value[subName];
                                fullSubName = name + '[' + subName + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        } else if (value !== undefined && value !== null) {
                            query += encodeURIComponent(name) + '='
                                + encodeURIComponent(value) + '&';
                        }
                    }

                    return query.length ? query.substr(0, query.length - 1) : query;
                };

                return angular.isObject(data) && String(data) !== '[object File]'
                    ? param(data)
                    : data;
            }];
        }]);
