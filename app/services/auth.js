/**
 * 服务
 */
angular.module('app')
    //权限管理
    .factory('AuthService', ['$http', '$q', 'ConfigService', 'localStorageService', '$state','$rootScope',
        function ($http, $q, ConfigService, localStorageService, $state,$rootScope) {
            var authService = {};
            //这里支持多库
            authService.getWid = function () {
                var dbInfo =localStorageService.get('db');
                if(dbInfo ){

                    var oDbInfo =JSON.parse(dbInfo);
                    return oDbInfo.dbid;
                }else{
                    return undefined;
                }
            };
            authService.login = function (credentials) {
                var deferred = $q.defer();
                var data = {"username": credentials.username, "password": credentials.password} ;
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/Login.action?do=login',
                    data: 'username=' + credentials.username + '&passwd=' + credentials.password +
                    '&verify=' + credentials.verify +'&wid=' +ConfigService.getWid(),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                })
                    .success(
                    function (data, status, header, config) {
                        if(data) {
                            console.log(data);
                            var userData = {
                                code: data.code,
                                token: data.sessionId,
                                msg: data.description,
                                balance: data.balance,
                                username: credentials.username,
                            };

                            if (data.code != '1') {
                                userData.msg = data.description;
                                deferred.reject(data);
                            }else{
                                $rootScope.Account = {username: userData.username, balance: userData.balance};
                                console.log($rootScope.Account);
                                localStorageService.add('User', JSON.stringify(userData,null, ' '));
                                deferred.resolve(data);
                            }
                        }else {
                            deferred.reject("ERR_LOGIN");
                        }
                    }
                );
                return deferred.promise;
            };
            // FIXME: debug comments authService.relogin,produce should be uncomment;
            authService.getCurrentUser = function () {

                var User = JSON.parse(localStorageService.get('User'));

                if (User) {
                    return User;
                } else{
                    authService.relogin();
                    return null;
                }
            };
            authService.checkUser = function (dRequest) {
                var deferred = $q.defer();
                var data = {"username": dRequest.username, "sid": dRequest.sid};
                $http({
                    method: 'POST',
                    url: ConfigService.getHost() + '/v1/user/checkuser',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    },
                    params: dRequest
                })
                    .success(
                    function (data, status, header, config) {
                        if (data.sid) {//登陆成功
                            deferred.resolve(userData);
                        } else
                            deferred.reject(data);
                    }
                );
                return deferred.promise;
            };

            authService.settleError = function (msg) {
                if (msg == '登陆超时' || msg == '请登录') {
                    localStorageService.remove('User');
                    $state.go('login');
                }
                alert(msg);
            };
            authService.isAuthenticated = function () {
                return !!0;
            };
            authService.relogin = function () {
                localStorageService.remove('User');
                $state.go('login');
            };
            authService.isAuthorized = function (authorizedRoles) {
                if (!angular.isArray(authorizedRoles)) {
                    authorizedRoles = [authorizedRoles];
                }
                return true; //(authService.isAuthenticated() &&authorizedRoles.indexOf(Session.userRole) !== -1);
            };
            return authService;
        }]);
