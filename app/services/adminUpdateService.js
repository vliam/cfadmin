/**
 * 服务
 */
angular.module('app')

    .factory('AdminUpdateService',
        ['$http', '$q', 'ConfigService', 'localStorageService', 'AuthService',
            function ($http, $q, ConfigService, localStorageService, AuthService) {
                var adminUpdateService = {};

                //获取可以修改的类型
                adminUpdateService.getType = function (dRequest) {

                    var User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    console.log(dRequest);


                    $http({
                        method: 'POST',
                        url: ConfigService.getHost()
                        +'/admin/Userinfo.action;jsessionid='+ User.token + '?do=getUserType&wid=t1',
                        params: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;

                                if (data.code != 1) {
                                    deferred.reject(data);
                                } else {
                                    console.log(data);
                                    deferred.resolve(data);
                                }
                            },
                            function (data) {
                                console.log("user list error");
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };
                
                //获取修改信息
                adminUpdateService.getInfo = function (dRequest) {

                    var User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    console.log(dRequest);

                    $http({
                        method: 'POST',
                        url: ConfigService.getHost()
                        +'/admin/Userinfo.action;jsessionid='+ User.token + '?do=getMemberByUserId&wid=t1',
                        params: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;

                                if (data.code != 1) {
                                    deferred.reject(data);
                                } else {
                                    deferred.resolve(data);
                                }
                            },
                            function (data) {
                                console.log("user list error");
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };

                ///保存用户信息
                adminUpdateService.saveInfo = function (dRequest) {

                    var User = AuthService.getCurrentUser();
                    var deferred = $q.defer();
                    console.log(dRequest);

                    $http({
                        method: 'POST',
                        url: ConfigService.getHost()
                        +'/admin/Userinfo.action;jsessionid='+ User.token + '?do=updateMember&wid=t1',
                        params: dRequest,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        }
                    })
                        .then(
                            function (resp, status, header, config) {
                                data = resp.data;
                                console.log(data);
                                if (data.code != 1) {
                                    deferred.reject(data);
                                } else {
                                    deferred.resolve(data);
                                }
                            },
                            function (data) {
                                console.log("user list error");
                                deferred.reject(data);
                            }
                        );
                    return deferred.promise;
                };

                return adminUpdateService;
            }]);

