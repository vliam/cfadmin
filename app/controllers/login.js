'use strict';
app
    // Dashboard Box controller 
    .controller('LoginCtrl', [
        '$rootScope', '$scope', 'AuthService', 'md5', '$state','$location','T',
        function ($rootScope, $scope, AuthService, md5, $state, $location, T) {
            $scope.sample = "test";
            var User = AuthService.getCurrentUser();
            if (User) {
                $state.go("app.dashboard");
            }
            $rootScope.webInfo = {
                "host": $location.host(),
                "port" :$location.port()
            };

            $scope.login = function (credentials) {
                // console.log(credentials);
                var c = {username: '', password: '', verify: ''};
                c.username = credentials.usr;
                c.password = md5.createHash(credentials.psw);
                c.verify = md5.createHash(credentials.usr);
                AuthService.login(c).then(
                    function (user) {
                        console.log(user);
                        // $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        //$scope.setCurrentUser(user);
                        $state.go("app.dashboard");
                    }, function (msg) {
                        if (msg.description){
                            alert(msg.description);
                        }else{
                            alert(msg.code);
                        }
                        //$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                    }
                );
            };
            $scope.oi18n = T.getI18n("LoginCtrl");
            $scope.oi18n.txt  = T.txt;

        }//ctrl
    ]);