/*
 * There are three declared controllers in this file
 * 
 * 1. AdminCtrl
 * 2. AdminDetailCtrl
 * 3. AdminUpdateCtrl
 *
 * */
app
    .controller('AdminCtrl', [
        '$rootScope', '$scope', 'UserService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', 'AdminService', 'T',
        function ($rootScope, $scope, UserService, dateService, AuthService, $state, ConstService, $location, AdminService, T) {
            $scope.pager = {totalItems: 10, maxSize: 10};
            $rootScope.dRequest = {"page": 1, "pageSize": 10};
            $scope.oi18n = {txt : T.txt};
            /////////////////////////////////////
            // Authservice located at auth.js file
            // check if there is a current user, if not, it will go to login page
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }

            // may be it is not used on AdminCtrl View
            $scope.goDetail = function (sUserName) {
                $location.path('/UserPage/' + sUserName);
            };

            // parse string and convert to a new format
            $scope.mask = function (str){
                if (!str)return "";
                var aStr = str.split("|");
                var sRes = aStr[0].substring(0, 3) + "****" + aStr[0].substring(7, 11);
                if (aStr.length > 1) {
                    sRes += "|" + aStr[1];
                }
                return sRes;
            };
            $scope.getUserStatus = function (iStatus) {
                return ConstService.getUserStatus(iStatus);
            };

            // get the translated values from oi18n folder
            $scope.oi18n = T.getI18n("AdminCtrl");

            //console.log("melvin111",$scope.oi18n);
            
            // initialize the content of table head in admin list
            $scope.aTitle = [
                [$scope.oi18n.createTime, ''],
                [$scope.oi18n.user, ''],
                [$scope.oi18n.status, 'numeric'],
                [$scope.oi18n.type, '']
            ];

            //$scope.aStatus = [];
            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.pager = {totalItems: 10, maxSize: 10};

            $scope.showExtraButton = function() {
                $scope.isShowAllButton = true;
            };

            $scope.init = function () {
                console.log("init admin");
                $scope.dRequest.orderBy = 0;
                $scope.aStatus = [{i: 1, label: '活跃'}, {i: 2, label: '冻结'}];
                $scope.aType = [{i: 1, label: '普通用户'}, {i: 2, label: '测试用户'}];
                $scope.requestFunc = function () {
                    $scope.oSearch = ConstService.getSearchBar('adminList');
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";
                    AdminService.getList($rootScope.dRequest).then(
                        function (data) {
                            $scope.oData = data.data.pageContent;
                            console.log("rData",$scope.oData);
                            $scope.transStatus = [$scope.oi18n.normal, $scope.oi18n.freeze, $scope.oi18n.unavailable];
                            console.log("Trans-Status",$scope.transStatus);
                            $scope.transType = [T.txt('General User'), T.txt('CS'), T.txt('Agent'), '', T.txt('Administrator'), '', '',
                                                T.txt('Retail Agent'), T.txt('Super Administrator')];
                            $scope.bLoading = false;
                        });
                };
                $scope.requestFunc();
            };

            // event in "ADD ADMIN" button
            $scope.addAdmin = function () {
                $state.go('app.adminDetail');
            };

            // event in "MODIFY button" at ADMIN LIST INTERFACE with id as input
            $scope.updateAdmin = function (id) {
                $state.go('app.adminUpdate',{id: id});

            }
        }
    ])
    .controller('AdminDetailCtrl', [
        '$rootScope', '$scope', 'UserService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', '$stateParams', 'AdminDetailService', 'T',
        function ($rootScope, $scope, UserService, dateService, AuthService, $state, ConstService, $location, $stateParams, AdminDetailService, T) {

            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }


            $scope.oi18n = T.getI18n("AdminDetailCtrl");
            $scope.oi18n.txt = T.txt;
            $scope.aTitle = [

                [$scope.oi18n.userName, ''],
                [$scope.oi18n.password, ''],
                [$scope.oi18n.paypasswd, ''],
                [$scope.oi18n.type, '']

            ];

            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };


            $scope.menberInfo = {
                userName : "",
                password : "",
                type : ""
            };

            $scope.initType = function () {
                console.log("init type");

                $scope.typeSel = "2";

                $scope.requestFunc = function () {

                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";
                    AdminDetailService.getType($rootScope.dRequest).then(
                        function (data) {
                            var data = data.data;
                            $scope.types = data;
                            $scope.bLoading = false;
                        });
                };
                $scope.requestFunc();
            };


            $scope.addMembers = function () {

                $scope.requestFunc = function () {

                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    $scope.menberInfo.type = $scope.typeSel;
                    
                    AdminDetailService.addUser($scope.menberInfo).then(
                        function (data) {
                            console.log(data);
                            alert(data.description);
                        },function (data) {
                            console.log(data);
                            alert(data.description);
                        });
                };
                $scope.requestFunc();

            };

            $scope.clear = function () {
                $scope.menberInfo = {
                    userName : "",
                    password : "",
                    type : "",
                }
            }

        }
    ])
    .controller('AdminUpdateCtrl', [
        '$rootScope', '$scope', 'UserService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', '$stateParams', 'AdminUpdateService', 'AdminDelService', 'T',
        function ($rootScope, $scope, UserService, dateService, AuthService, $state, ConstService, $location, $stateParams, AdminUpdateService, AdminDelService, T) {

            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }

            $scope.oi18n = T.getI18n("AdminUpdateCtrl");
            $scope.oi18n.txt = T.txt;
            $scope.aTitle = [
                [$scope.oi18n.userName, ''],
                [$scope.oi18n.createTime, ''],
                [$scope.oi18n.superior, ''],
                [$scope.oi18n.type, '']
            ];

            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };

            // initialize user details based on the id
            $scope.initUpdateInfo = function () {
                console.log("update");

                $scope.requestFunc = function () {

                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    $scope.userInfo = {
                        id : $stateParams.id
                    };

                    AdminUpdateService.getType($scope.userInfo).then(
                        function (data) {
                            var data = data.data;
                            $scope.types = data;
                            $scope.bLoading = true;
                            $scope.sLoadingInfo = T.txt('Loading') + "...";

                            AdminUpdateService.getInfo($scope.userInfo).then(
                                function (data) {
                                    var data = data.data;

                                    $scope.adminInfo = data;
                                    $scope.typeSel = $scope.adminInfo.type;
                                    $scope.bLoading = false;

                                });
                        });

                };
                $scope.requestFunc();
            };

            // event to save button when clicked
            $scope.saveInfo = function () {
                console.log("save");

                $scope.requestFunc = function () {

                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    $scope.menberInfo ={
                        id : $stateParams.id,
                        status : $scope.adminInfo.status,
                        password : $scope.updatePSW,
                        type : $scope.typeSel
                    };

                    AdminUpdateService.saveInfo($scope.menberInfo).then(
                        function (data) {
                            alert(data.description);
                        },function (data) {
                            alert(data.description);
                        });
                };

                $scope.requestFunc();

            };

            // event to delete button when clicked
            $scope.delete = function () {
                console.log("delete");

                $scope.requestFunc = function () {

                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    $scope.menberInfo ={
                        userName : $scope.adminInfo.username
                    };

                    AdminDelService.DelAdmin($scope.menberInfo).then(
                        function (data) {
                            alert(data.description);
                        },function (data) {
                            alert(data.description);
                        });
                };

                $scope.requestFunc();

            }

        }
    ])

