"use strict";

app.controller('LanguageSwitchingCtrl', ['$scope', '$translate', '$location', function ($scope, $translate, $location) {
    /*
     $scope.switching = function(lang){
     $translate.use(lang);
     window.localStorage.lang = lang;
     window.location.reload();
     };
     $scope.cur_lang = $translate.use();
     */


    $scope.switching = function (lang) {
        console.log(lang);
        $translate.use(lang);
        window.localStorage.lang = lang;
        window.location.reload();
    };


    $scope.cur_lang = $translate.use();

    $scope.initLang = function () {

        if (!window.localStorage.lang) {
            var sUrl = $location.url();
            var lang = 'en_US';
            if (sUrl.indexOf('_CN') > 0) {
                lang = 'zh_CN';
            } else if (sUrl.indexOf('_EN') > 0) {
                lang = 'en_US';
            } else if (sUrl.indexOf('_TW') > 0) {
                lang = 'zh_TW';
            }
            $scope.switching(lang);
        } else {
            $scope.switching(window.localStorage.lang);
        }
    }

}])
    .controller('DashboardCtrl', [
        '$rootScope', '$scope', 'dateService', 'AuthService', '$state', 'ReportService', 'ConstService', 'T', 'SearchBarService',
        function ($rootScope, $scope, dateService, AuthService, $state, ReportService, ConstService, T, SearchBarService) {

            // 获得搜索栏
            // $rootScope.oSearch = {
            //     isGameType: 1
            // }

            $scope.oSearch = ConstService.getSearchBar('dashboard');
            $rootScope.dRequest = { num: 30 };

            // if ($scope.oSearch.gameTypeArray) {
            //     console.log("oSearchDashboard", $scope.oSearch.gameTypeArray); 
            //     $rootScope.dRequest.gameType = $scope.oSearch.gameTypeArray.type; 
            // }


            $scope.getReportView = function (dRequest) {
                console.log('dashboard get report');
                $scope.bLoading = true;
                $scope.sLoadingInfo = T.txt('Loading') + "...";

                $scope.oi18n = T;
                // $scope.oi18n  = T.txt;
                // $scope.oi18n = {txt:T.txt};
                ReportService.getReportView(dRequest).then(
                    function (data) {
                        console.log("%cLoad Event", "color:#ffff00");
                        $scope.oData = data.data;
                        //console.log($scope.oData); 
                        $scope.oToday = $scope.oData[$scope.oData.length - 1];
                        $scope.oYesterday = $scope.oData[$scope.oData.length - 2];
                        if (($scope.oYesterday != undefined) && ($scope.oYesterday.valid_amount != 0)) {
                            $scope.oToday.deltaAmount = parseInt(($scope.oToday.valid_amount - $scope.oYesterday.valid_amount) / $scope.oYesterday.valid_amount * 100);
                        } else {
                            $scope.oToday.deltaAmount = $scope.oToday.valid_amount > 0 ? 100 : 0;
                        }
                        if (($scope.oYesterday != undefined) && ($scope.oYesterday.userNum != 0)) {
                            $scope.oToday.deltaUser = parseInt(($scope.oToday.userNum - $scope.oYesterday.userNum) / $scope.oYesterday.userNum * 100);
                        } else {
                            $scope.oToday.deltaUser = $scope.oToday.userNum > 0 ? 100 : 0;
                        }
                        if (($scope.oYesterday != undefined) && ($scope.oYesterday.deposit != 0)) {
                            $scope.oToday.deltaDeposit = parseInt(($scope.oToday.deposit - $scope.oYesterday.deposit) / $scope.oYesterday.deposit * 100);
                        } else {
                            $scope.oToday.deltaDeposit = $scope.oToday.deposit > 0 ? 100 : 0;
                        }
                        if (($scope.oYesterday != undefined) && ($scope.oYesterday.withdraw != 0)) {
                            $scope.oToday.deltaWithdraw = parseInt(($scope.oToday.withdraw - $scope.oYesterday.userNum) / $scope.oYesterday.withdraw * 100);
                        } else {
                            $scope.oToday.deltaWithdraw = $scope.oToday.withdraw > 0 ? 100 : 0;
                        }
                        $scope.aAmount = [];
                        $scope.aUserNum = [];
                        $scope.aDeposit = [];
                        $scope.aWithdraw = [];
                        for (var i = 8; i >= 1; i--) {
                            $scope.aAmount.push($scope.oData[$scope.oData.length - i].valid_amount);
                            $scope.aUserNum.push($scope.oData[$scope.oData.length - i].userNum);
                            $scope.aDeposit.push($scope.oData[$scope.oData.length - i].deposit);
                            $scope.aWithdraw.push($scope.oData[$scope.oData.length - i].withdraw);
                        }
                        if ($scope.aUserNum[0] == undefined) {
                            $scope.sLoadingInfo = T.txt("No Data");
                        } else {
                            $scope.bLoading = false;
                        }

                    }
                );
            };
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }

            /* START: select option gameType */
            const gameTypeLabel = {
                newBomb: "ticash",
                dice: "dice",
                coin: "coin",
                sm: "spaceman",
                mb: "bobworker",
                fruit: "fruit"
            };

            $scope.gameTypeArray = [
                { type: "", code: "", label: T.txt('All Games') },
            ];

            SearchBarService.getLotteryCodeList().then(function (data) {
                var lotteryCodeData = data.data;
                // console.info("lotteryCodeLists", lotteryCodeData.data); 

                for (var key in lotteryCodeData.data) {
                    $scope.gameTypeArray.push({
                        type: lotteryCodeData.data[key].code,
                        // code: lotteryCodeData.data[key].name, 
                        label: gameTypeLabel[lotteryCodeData.data[key].name]
                    });
                }

            });

            if (!$scope.oSearch.gameTypeArray) {
                $scope.oSearch.gameTypeArray = $scope.gameTypeArray[0];
            }
            /* END: select option gameType */


            $scope.requestFunc = function () {
                $scope.bLoading = true;

                // console.log("全站：",$rootScope.dRequest);
                // get gameTypeArray
                if ($scope.oSearch.gameTypeArray) {
                    console.log("reqfuesFunction - DashboardCtrl", $scope.oSearch.gameTypeArray);
                    $rootScope.dRequest.gameType = $scope.oSearch.gameTypeArray.type;
                }

                $scope.getReportView($rootScope.dRequest);

                // ReportService.getStatistics($rootScope.dRequest).then(
                //     function (data) {
                //         // console.log(data);
                //         console.log("%cSearch Button", "color:#ffff00"); 
                //         var res = data.data;
                //         $scope.dbNameList = [];
                //         for(var i = 0 ; i < res.length; i++){
                //             if(res[i].status =="1"){
                //                 $scope.dbNameList.push(res[i]);
                //             }
                //         }
                //         if (!$rootScope.dbNameOp ){
                //             var localDb = localStorageService.get('db');
                //             if (!localDb){
                //                 $rootScope.dbNameOp = res[0];
                //                 localStorageService.set('db', JSON.stringify($rootScope.dbNameOp));
                //             }else{
                //                 $rootScope.dbNameOp = JSON.parse(localDb);
                //             }
                //         }
                //         $rootScope.User = {username :data.myInfo.username } ;
                //         if (data.myInfo.level >=8 ){
                //             $rootScope.authority.lockUser = true;

                //         }
                //         if (data.myInfo.level >=9 ){
                //             $rootScope.authority.credit = true;
                //             $rootScope.authority.cancelOrder = true;
                //         }
                //         console.log($scope.dbNameList);
                //     }
                // );
            };
            // $scope.requestFunc();


            $scope.aReg = [0];
            $scope.bLoadReg = false;
            $scope.nFinish = 0;
            $scope.nTotal = -1;
            $scope.init = function () {
                console.log('daily report');
                $scope.getReportView({ num: 30 });

            };

            ////////////////////
            //获得图表信息：
            //今日注册人数，今日提款人数，今日消费，总会员数
        }
    ])
    // 用户日结 User Daily
    .controller('ReportCtrl', [
        '$rootScope', '$scope', 'ReportService', 'dateService', 'AuthService', '$state', 'ConstService', '$filter', '$stateParams', 'T', 'SearchBarService',
        function ($rootScope, $scope, ReportService, dateService, AuthService, $state, ConstService, $filter, $stateParams, T, SearchBarService) {
            $scope.pager = { totalItems: 10, maxSize: 10, type: "" };
            $rootScope.dRequest = { "page": 1, "pageSize": 20 };

            // console.log('数据统计 Statistics');
            // 获得搜索栏
            // $rootScope.oSearch = {
            //     isGameType: 1
            // }

            $rootScope.oSearch = ConstService.getSearchBar('report');

            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            $scope.aTitle = [
                ['时间', ''],
                ['用户名', ''],
                ['金额', 'numeric'],
                ['说明', '']
            ];

            /* START: select option gameType */
            const gameTypeLabel = {
                0: "All Games",
                5: "ticash",
                6: "dice",
                7: "coin",
                8: "spaceman",
                9: "bobworker",
                10: "fruitcard"
            };

            $scope.gameTypeArray = [
                { type: "", code: "", label: T.txt('All Games') },
            ];

            SearchBarService.getLotteryCodeList().then(function (data) {
                var lotteryCodeData = data.data;
                // console.info("lotteryCodeLists", lotteryCodeData.data); 

                for (var key in lotteryCodeData.data) {
                    $scope.gameTypeArray.push({
                        type: lotteryCodeData.data[key].code,
                        code: lotteryCodeData.data[key].name,
                        label: gameTypeLabel[lotteryCodeData.data[key].code]
                    });
                }
            });
            // if(!$scope.oSearch.gameTypeArray) {
            //     $scope.oSearch.gameTypeArray = $scope.gameTypeArray[0];  
            // }
            $scope.oSearch.gameTypeArray = $scope.gameTypeArray[0];
            /* END: select option gameType */


            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function (e) {
                if (e != null) {
                    // console.log("取得翻页的数据Receive pager param:" + angular.toJson(e));
                }
                $scope.requestFunc(e, 1); // if the second args = 1 don't !order
            };
            //user report的时候调用
            $scope.oi18n = T.getI18n("ReportCtrl");
            $scope.oi18n.txt = T.txt;
            $scope.round = function (n) {
                return Math.round(n * 100) / 100;
            };

            // Node clicked - ChangeClass / it is used to change the icon symbol for + or -
            $scope.changeNodeClassValue = function (NodeName) {
                return NodeName += 1;
            };

            $scope.displayExportButton = function () {    // User Daily show Button also the showAllButton
                $scope.isExportButton = true;
                $rootScope.QueryType = "userdaily";
                $scope.isShowAllButton = true;
            };

            // initUserReport 用户日结 User Daily;
            $scope.initUserReport = function (bNewOnly) { // UserDaily Initialization function
                if (bNewOnly != null) {
                    $rootScope.dRequest.status = 1;
                }
                // 获得搜索栏
                // $scope.oSearch = ConstService.getSearchBar('report');


                $scope.aTitle = [
                    [$scope.oi18n.user, ''],
                    [$scope.oi18n.betting, ''],
                    [$scope.oi18n.effectBet, ''],
                    [$scope.oi18n.bonus, ''],
                    [$scope.oi18n.profit, ''],
                    [$scope.oi18n.deposit, 'numeric'],
                    [$scope.oi18n.withdraw, 'numeric'],
                ];

                $rootScope.dRequest.type = 0;

                $rootScope.dRequest.gameType = "";    // ""=>所有游戏 // 5=>newbomb  6=>dice

                $rootScope.dRequest.sortnum = 1; // default orderby id(1) desc,
                $rootScope.dRequest.order = 1; //0-正序asc 1-倒序desc;

                $scope.requestFunc = function (e, pageSwitch) {

                    if ($scope.oSearch.gameTypeArray) {
                        console.log('用户日结 $rootScope.oSearch.gameTypeArray', $scope.oSearch.gameTypeArray);
                        $rootScope.dRequest.gameType = $scope.oSearch.gameTypeArray.type;
                    }

                    if (e != null) {
                        if (pageSwitch != 1) { // !=1 no paging, 1=switch paging
                            $rootScope.dRequest.order = !$rootScope.dRequest.order;
                        }
                        e.order = $rootScope.dRequest.order == true ? 1 : 0;
                        //console.log("args:" + angular.toJson(e)); // {"sortnum":4,"order":1}
                        //console.log("sortnum: " + e.sortnum + " order: " + e.order);
                        //console.groupEnd();
                        $rootScope.dRequest.sortnum = e.sortnum;
                        $rootScope.dRequest.order = e.order;

                        $rootScope.dRequest.tempPagePamas = e; // save [e] obj to temp for pageer.html function 保存临时翻页数据;
                        console.log("参数:" + angular.toJson(e)); // {"sortnum":4,"order":1}
                    }
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    if ($stateParams.userName) {
                        $rootScope.dRequest.userName = $stateParams.userName;
                    }
                    ReportService.findUserDailyReportList($rootScope.dRequest).then(
                        function (data) {
                            // console.group("用户日结");
                            // console.info("用户日结", data);
                            // console.groupEnd();
                            //这里是数据，数组每一条为一行
                            $scope.oUser = data.userData[0];
                            $scope.oData = data.subData;
                            $scope.pager.totalItems = data.itemTotal;
                            if ($scope.oData.length + data.userData.length == 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        }
                    );
                };
                $scope.requestFunc(); // init
            };
            $scope.initAgentReport = function (bNewOnly) {
                if (bNewOnly != null) {
                    $rootScope.dRequest.status = 1;
                }
                $scope.oSearch = ConstService.getSearchBar('date');

                $scope.aTitle = [
                    [$scope.oi18n.agent, ''],
                    [$scope.oi18n.betting, ''],
                    [$scope.oi18n.effectBet, ''],
                    [$scope.oi18n.bonus, ''],
                    [$scope.oi18n.deposit, 'numeric'],
                    [$scope.oi18n.withdraw, 'numeric'],
                ];
                $rootScope.dRequest.type = 0;
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    ReportService.findAllAgentDailyList($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            //这里是数据，数组每一条为一行
                            $scope.oData = data.agentData;
                            $scope.pager.totalItems = data.itemTotal;
                            if ($scope.pager.totalItems == 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        }, function (data) {
                            console.log(data);
                        }
                    );
                };
                $scope.requestFunc();
            };
            /////////////////////////////////////////////////////////////////////////////
            $scope.initUserCredit = function (bNewOnly) {
                $scope.oi18n = T.txt;
                if (bNewOnly != null) {
                    $rootScope.dRequest.status = 1;
                }
                $scope.oSearch = { hideUserName: true, hideBtn: true };

                //                $scope.aTitle = [
                //                    [$scope.oi18n.agent, ''],
                //                    [$scope.oi18n.bonus, ''],
                //                    [$scope.oi18n.bet_amount, ''],
                //                    [$scope.oi18n.valid_amout, ''],
                //                    [$scope.oi18n.deposit, 'numeric'],
                //                    [$scope.oi18n.withdraw, 'numeric'],
                //                ];
                //$rootScope.dRequest.type = 0;

                $scope.requestFunc = function (oParam) {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";
                    //todo:using fake data for testing
                    ReportService.creditService(
                        oParam
                    ).then(
                        function (data) {
                            //这里是数据，数组每一条为一行
                            $scope.oData = data;
                            console.log($scope.oData);
                            $scope.pager.totalItems = data.itemTotal;
                            if ($scope.pager.totalItems == 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        }, function (data) {
                            console.log(data);
                        }
                    );
                };
                $scope.requestFunc({ type: 0, amount: 0 });//page loading
            };

            $scope.amount = 100;
            $scope.clickAdd = function () {
                var result = confirm($scope.oi18n('Are you sure?'));
                console.log("will add :" + $scope.amount);
                console.log(result);
                if (result === true) {
                    $scope.requestFunc({ type: 1, amount: $scope.amount });//add amount
                }
            }

            $scope.clickReset = function () {
                var result = confirm($scope.oi18n('Are you sure?'));
                if (result === true) {
                    $scope.requestFunc({ type: 2, amount: 0 });//reset
                }
            }

            //***  数据统计 Statistics ***
            $scope.initAllUser = function () {
                // $rootScope.oSearch = {
                //     isGameType:1
                // }

                $scope.oSearch = ConstService.getSearchBar('date');
                $scope.oSearch.dSymbol = true;
                $scope.currentSymbol = 'CNY';
                $scope.selectSymbol = function (sSymbol) {
                    $scope.currentSymbol = sSymbol;
                    $scope.requestFunc();
                };
                $scope.aTitle = [
                    [$scope.oi18n.website, ''],
                    [$scope.oi18n.currency, ''],
                    [$scope.oi18n.bet, ''],//投注
                    [$scope.oi18n.effectBet, ''],//有效
                    [$scope.oi18n.bonus, ''],//奖金
                    [$scope.oi18n.deposit, 'numeric'],//存款
                    [$scope.oi18n.withdraw, 'numeric'],//提款
                    [$scope.oi18n.profit, '']//盈利
                ];
                $rootScope.dRequest.type = 0;
                $scope.treeData = {};// this is the root node

                /*
                input the data from service
                make it to the tree object
                */
                var transData2Tree = function (data) {

                    var rootData = data.data;
                    var subdata = data.subdata;

                    var nUnSettled = 0;
                    var sWhoHasChild = "";
                    var sSubNames = "";

                    for (var i = 0; i < subdata.length; i++) {
                        nUnSettled++;// count how many node that hasn't been settled

                        sWhoHasChild = sWhoHasChild + "|" + subdata[i].superior;
                        sSubNames = sSubNames + "|" + subdata[i].agentName + "|";
                        // console.log("subdata", subdata[i]);
                    }
                    for (var i = 0; i < subdata.length; i++) {
                        if (sSubNames.indexOf("|" + subdata[i].superior + "|") < 0) { //this superior is not in the SubName list
                            rootData.name = subdata[i].superior;
                        }
                    }
                    console.log("rootData", rootData);
                    var iTry = 0;
                    while (nUnSettled > 0 && iTry < 100) {
                        iTry++;
                        //every time settle the leaves,
                        // leaves == the node who didn't appear in the superior(has no child)
                        for (var i = 0; i < subdata.length; i++) {
                            for (var j = 0; j < subdata.length; j++) {
                                //if subdata[j] is not leaf , skip
                                //every time, only check the nodes who has no child
                                //once a node is settled, it's superior =""
                                //next round, we will consider it's superior as a new leaf
                                if (sWhoHasChild.indexOf(subdata[i].agentName < 0)) {//im a leaf
                                    //debugger
                                    if (subdata[j].superior == subdata[i].agentName) {//j is i's child
                                        if (!subdata[i].childArray) {
                                            subdata[i].childArray = [];
                                        }
                                        subdata[i].childArray.push(subdata[j]);
                                        subdata[j].superior = "";//mark that this node has been settled
                                    } else { // check if j is root' child
                                        if (subdata[j].superior == rootData.name) {//j's superior is root
                                            if (!rootData.childArray) {
                                                rootData.childArray = [];
                                            }
                                            rootData.childArray.push(subdata[j]);
                                            subdata[j].superior = "";//mark that this node has been settled
                                        }//end of check root
                                    }//end check child
                                }//end check leaf
                            }
                        }
                        sWhoHasChild = "";// reset
                        nUnSettled = 0;//recount
                        for (var i = 0; i < subdata.length; i++) {
                            if (subdata[i].superior != "") {//
                                nUnSettled++;
                            }
                            sWhoHasChild = sWhoHasChild + "|" + subdata[i].superior;
                        }//end for
                    }//end while

                    $scope.treeData = rootData;
                    console.log("treeData", $scope.treeData);
                };

                // * 数据统计 接收
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    if ($scope.oSearch.gameTypeArray) {
                        console.log('数据统计查询 $rootScope.oSearch.gameTypeArray', $scope.oSearch.gameTypeArray);
                        $rootScope.dRequest.gameType = $scope.oSearch.gameTypeArray.type;
                    }

                    //console.log("数据统计查询 $rootScope.dRequest.gameType",$rootScope.dRequest.gameType);
                    ReportService.findAllUserDailyList($rootScope.dRequest).then(
                        function (data) {
                            data = data;

                            var fdata = data.data;

                            //这里是数据，数组每一条为一行
                            transData2Tree(data);
                            $scope.oData = [];
                            console.log("$scope.oData", $scope.oData);
                            var j = 0;
                            for (var i = 0; i < fdata.length; i++) {
                                if (!fdata[i] || fdata[i].currency != $scope.currentSymbol) {
                                    continue;
                                }
                                $scope.oData.push(fdata[i].summary);
                                $scope.oData[j].currency = fdata[i].currency;
                                $scope.oData[j].merchantName = fdata[i].merchantName;
                                $scope.oData[j].profit = $scope.oData[j].valid_amount - $scope.oData[j].bonus;
                                j++;
                            }
                            $scope.oUser = fdata.summary;
                            $scope.pager.totalItems = fdata.itemTotal;
                            if ($scope.pager.totalItems === 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        }, function (data) {
                            console.log(data);
                        }
                    );
                };
                $scope.requestFunc();
            };


        }
    ])
    .controller('AccountCtrl', [
        '$rootScope', '$scope', 'AccountDetailService', 'dateService', 'AuthService', '$state', 'ConstService', '$filter', '$stateParams', 'T',
        function ($rootScope, $scope, AccountDetailService, dateService, AuthService, $state, ConstService, $filter, $stateParams, T) {

            $scope.pager = { totalItems: 10, maxSize: 10, type: "" };
            $rootScope.dRequest = { "page": 1, "pageSize": 20 };
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            console.log('getCurrentUser');
            console.log($rootScope.User);
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            $scope.aTitle = [
                ['时间', ''],
                ['用户名', ''],
                ['金额', 'numeric'],
                ['说明', '']
            ];

            //这个页面是详情还是导航
            if ($stateParams.userName && $stateParams.userName.length > 0) {
                $rootScope.userName = $stateParams.userName;
                $scope.bDetail = true;
                $rootScope.dRequest.userName = $stateParams.userName;
            }

            // check if userNameParams parameter is exists on the url parameter
            // since the dRequest variable is used in the first load of the requestFunc, there is nothing to do, just put the parameter inside the dRequest
            if (($stateParams.userNameParams) && ($stateParams.userNameParams.length > 0)) {
                $rootScope.dRequest.userName = $stateParams.userNameParams;
            }

            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.initBalance = function (type) {
                if (type) {
                    $rootScope.dRequest.type = type;
                }

                $scope.aTitle = [
                    ['创建时间/更新时间', ''],
                    ['用户名', ''],
                    ['金额', 'numeric'],
                    ['当前余额', 'numeric'],
                    ['备注', ''],
                    ['订单号', ''],
                    ['说明', '']
                ];

                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    AccountDetailService.getList($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            if ($scope.pager.totalItems == 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        }
                    );
                };
                $scope.requestFunc();
            };

            /////////////////////////////////////////////////////////////////////

            $scope.oi18n = T.getI18n("AccountCtrl");

            $scope.initBusiness = function (oChecker) {
                console.group("init Business");
                console.log(oChecker);
                $scope.oSearch = ConstService.getSearchBar('businessList');
                if (oChecker && oChecker.showExport) {
                    $scope.isExportButton = true;
                    $rootScope.QueryType = "account";
                }
                $scope.aTitle = [
                    [$scope.oi18n.CreateTime, ''],
                    [$scope.oi18n.orderId, ''],
                    [$scope.oi18n.user, ''],
                    [$scope.oi18n.beforeAmount, ''],
                    [$scope.oi18n.afterAmount, ''],
                    [$scope.oi18n.withdrawAmount, ''],
                    [$scope.oi18n.description, '']
                ];

                $scope.aStatus = [{ i: 0, label: '初始' }, { i: 1, label: '审核中' }, { i: 2, label: '已打款' }, {
                    i: 3,
                    label: '已拒绝'
                }];

                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    AccountDetailService.getBusiness($rootScope.dRequest).then(
                        function (data) {
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            for (var i = 0, len = $scope.oData.length; i < len; i++) {
                                if ($scope.oData[i].memo == '投注返点') {
                                    $scope.oData[i].memoLang = T.txt('Bet Rebate');
                                } else if ($scope.oData[i].memo == '开空撤单') {
                                    $scope.oData[i].memoLang = T.txt('Withdraw the order');
                                } else if ($scope.oData[i].memo == '系统返奖') {
                                    $scope.oData[i].memoLang = T.txt('System Rewards');
                                } else if ($scope.oData[i].memo == '撤单返款') {
                                    $scope.oData[i].memoLang = T.txt('Cancel return');
                                } else if ($scope.oData[i].memo == '投注扣款') {
                                    $scope.oData[i].memoLang = T.txt('Betting charge');
                                } else if ($scope.oData[i].memo == '账户转出') {
                                    $scope.oData[i].memoLang = T.txt('Account transferred out');
                                } else if ($scope.oData[i].memo == '账户转入') {
                                    $scope.oData[i].memoLang = T.txt('Account transfer');
                                } else {
                                    $scope.oData[i].memoLang = $scope.oData[i].memo;
                                }
                            }

                            if ($scope.pager.totalItems == 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        }
                    );
                };
                console.groupEnd();
                $scope.requestFunc();
            };
            $scope.showExtraButtons = function () {
                $scope.isShowAllButton = true;
            };
            $scope.initDeposit = function (bNewOnly) {
                if (bNewOnly != null) {
                    $rootScope.dRequest.status = 1;
                }
                $scope.aStatus = [{ i: 0, label: "初始" }, { i: 1, label: "已充值" }];
                $scope.oSearch = ConstService.getSearchBar('depositList');
                $scope.aFs = [{ key: "bf", label: "宝付" }, { key: "pnrPay", label: "PNR网银" }, { key: "zfb", label: "支付宝" }];

                $scope.aTitle = [
                    [$scope.oi18n.CreateTime, ''],
                    [$scope.oi18n.orderId, ''],
                    [$scope.oi18n.user, ''],
                    [$scope.oi18n.beforeAmount, ''],
                    [$scope.oi18n.afterAmount, ''],
                    [$scope.oi18n.rechargeAmount, ''],
                    [$scope.oi18n.description, '']
                ];

                $rootScope.dRequest.type = 203;
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    AccountDetailService.getDeposit($rootScope.dRequest).then(
                        function (data) {

                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            if ($scope.pager.totalItems == 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        }
                    );
                };
                $scope.requestFunc();
            };
            $scope.initWithdraw = function () {
                $scope.oSearch = ConstService.getSearchBar('withdrawList');

                $scope.aTitle = [
                    [$scope.oi18n.CreateTime, ''],
                    [$scope.oi18n.orderId, ''],
                    [$scope.oi18n.user, ''],
                    [$scope.oi18n.beforeAmount, ''],
                    [$scope.oi18n.afterAmount, ''],
                    [$scope.oi18n.withdrawAmount, ''],
                    [$scope.oi18n.description, '']
                ];
                $scope.aStatus = [{ i: 0, label: '初始' }, { i: 1, label: '审核中' }, { i: 2, label: '已打款' }, {
                    i: 3,
                    label: '已拒绝'
                }];

                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";
                    $rootScope.dRequest.type = 202;

                    AccountDetailService.getWithdraw($rootScope.dRequest).then(
                        function (data) {
                            // console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            if ($scope.pager.totalItems == 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        }
                    );
                };
                $scope.requestFunc();
            };


        }
    ])
    .controller('UserDetailCtrl', [
        '$rootScope', '$scope', 'UserService', 'dateService', 'AuthService', '$state', 'ConstService', '$stateParams',
        function ($rootScope, $scope, UserService, dateService, AuthService, $state, ConstService, $stateParams) {
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            $scope.getRegFrom = function (i) {
                return ConstService.getRegFrom(i);
            };
            if (!$scope.User) {
                $state.go("login");
                console.log("user error");
            }
            console.log($stateParams);
            if ($stateParams.userName != null && $stateParams.userName.length > 0) {
                $rootScope.userName = $stateParams.userName;
            }
            $scope.getUserStatus = function (iStatus) {
                return ConstService.getUserStatus(iStatus);
            };
            //$scope.aStatus = [];
            $scope.init = function () {//用户详情单页，在这里加载用户的信息
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    var request = {
                        username: $stateParams.userName
                    };
                    UserService.getDetail(request).then(
                        function (data) {
                            $scope.data = data;
                            if (!$scope.data.member.username) {
                                $scope.data.member = eval('(' + data.member + ')');
                            }
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
        }
    ])

    /*****  获奖订单 Order Win 10.25.2018 ******/
    .controller('OrderCtrl', [
        '$rootScope', '$scope', 'OrderService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', '$stateParams', 'T', 'SearchBarService',
        function ($rootScope, $scope, OrderService, dateService, AuthService, $state, ConstService, $location, $stateParams, T, SearchBarService) {

            $scope.pager = { totalItems: 10, maxSize: 10 };
            $rootScope.dRequest = { "page": 1, "pageSize": 20 };
            if ((($stateParams.inputParams) && ($stateParams.inputParams.length > 0)) && (isNaN($stateParams.inputParams))) {//single user page
                $rootScope.userName = $stateParams.inputParams;
                //$scope.bDetail =true;
                $rootScope.dRequest.username = $stateParams.inputParams;
                console.log("username Detected");
            } else {
                $rootScope.dRequest.orderNo = $stateParams.inputParams;
                console.log("order ID Detected");
            }
            $rootScope.dRequest.queryflag = true;

            ////////// check login //////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }



            /* START: select option gameType */
            $scope.oSearch = ConstService.getSearchBar('orderList');
            const gameLotteryCode = {
                newBomb: { label: "ticash", code: "newBomb" },
                dice: { label: "dice", code: "dice" },
                coin: { label: "coin", code: "coin" },
                sm: { label: "spaceman", code: "sm" },
                mb: { label: "bobworker", code: "mb" },
                fruit: { label: "fruitcard", code: "fruit" }
            };

            $scope.gameTypeArray = [
                { type: "", code: "", label: T.txt('All Games') },
            ];

            SearchBarService.getLotteryCodeList().then(function (data) {
                var lotteryCodeData = data.data;
                console.info("lotteryCodeLists", lotteryCodeData.data);

                for (var key in lotteryCodeData.data) {
                    $scope.gameTypeArray.push({
                        type: lotteryCodeData.data[key].code,
                        code: gameLotteryCode[lotteryCodeData.data[key].name].code,
                        label: gameLotteryCode[lotteryCodeData.data[key].name].label
                    });
                }
                // if(!$scope.oSearch.gameTypeArray) { $scope.oSearch.gameTypeArray = $scope.gameTypeArray[1]; } 
                $scope.oSearch.gameTypeArray = $scope.gameTypeArray[$rootScope.reportHistorySearchGameIndex];
                console.info("%coSearch.gameTypeArray", "color:#ffff00", $scope.oSearch.gameTypeArray);
                $scope.requestFunc();
            });

            $scope.getGameType = function (e) {
                return T.txt(gameLotteryCode[e].label);
            }
            /* END: select option gameType */





            // 游戏
            // $rootScope.dRequest.gameType

            $scope.oi18n = T.getI18n("OrderCtrl");

            $scope.aTitle = [
                [$scope.oi18n.time, ''],
                [$scope.oi18n.raceNum, ''],
                [$scope.oi18n.user, ''],
                [$scope.oi18n.amount, ''],
                ['@', ''],
                [$scope.oi18n.winAmount, ''],
                [$scope.oi18n.arena, ''],
                [$scope.oi18n.orderNum, ''],
                [$scope.oi18n.state, ''],
                [$scope.oi18n.odds, '']
            ];
            //$scope.aStatus = [];
            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.pager = { totalItems: 10, maxSize: 10 };
            $scope.init = function (oParams) {

                if (oParams && oParams.bWinOnly) { // Winning orders
                    $scope.dRequest.orderStatus = 2;
                    $scope.dRequest.secAgo = 0;
                    $scope.isExportButton = true;
                    $rootScope.QueryType = "order";
                    $scope.isShowAllButton = true;
                }
                if (oParams && oParams.isQT) {
                    $scope.dRequest.dataType = 1;
                    $scope.dRequest.secAgo = 0;
                }
                if (oParams && oParams.pending) {
                    $scope.dRequest.orderStatus = 0;
                    $scope.dRequest.secAgo = 120;
                    $scope.isShowAllButton = true;
                }
                if (oParams && oParams.orderListExport) { // Betting Record
                    $scope.isExportButton = true;
                    $rootScope.QueryType = "order";
                    $scope.isShowAllButton = true;
                }
                $scope.dRequest.orderBy = 0;

                /* 游戏种类 移到 searchBarCtrl */
                // $scope.gameType = [
                //     {i: 0, label: T.txt('All Games')}, //所有游戏
                //     {i: 5, label: T.txt('Tick Cash')},  // newbomb
                //     {i: 6, label: T.txt('Magic Dice')},     // dice
                // ];

                /* 游戏状态*/
                $scope.aStatus = [{ i: 0, label: T.txt('Initial') },
                { i: 1, label: T.txt('Missed') },
                { i: 2, label: T.txt('Won') },
                { i: 5, label: T.txt('Refund') },
                { i: 6, label: T.txt('Canceled By Admin') },
                { i: 7, label: T.txt("Tie Refund") }
                ];
                $scope.aType = [{ i: 0, label: '用户' }, { i: 1, label: '测试用户' }];


                /* submit */
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    /* 游戏种类*/
                    // console.log($rootScope.dRequest.gameType);
                    // console.groupEnd();

                    /*订单状态*/
                    if ($rootScope.dRequest.status) {
                        $rootScope.dRequest.orderStatus = $rootScope.dRequest.status;
                    }

                    if ($rootScope.dRequest.userName) {
                        $rootScope.dRequest.username = $rootScope.dRequest.userName;
                        $rootScope.dRequest.queryflag = true;
                    }

                    console.log('$scope.oSearch', $scope.oSearch);
                    /* 游戏种类*/
                    if ($scope.oSearch.gameTypeArray) {
                        console.log("oSearch.gameTypeArray", $scope.oSearch.gameTypeArray);

                        for (var key in $scope.gameTypeArray) {
                            if ($scope.gameTypeArray[key].type === $scope.oSearch.gameTypeArray.type) {
                                $rootScope.reportHistorySearchGameIndex = key;
                                break;
                            }
                        }
                        $rootScope.dRequest.lotteryCode = $scope.oSearch.gameTypeArray.code;
                    }
                    console.log("$rootScope.dRequest", $rootScope.dRequest.lotteryCode);
                    console.groupEnd();

                    OrderService.getList($rootScope.dRequest).then(
                        function (data) {

                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            // console.groupCollapsed("OrderService");
                            // console.log(data);
                            // console.groupEnd();
                            $scope.transStatus = [T.txt('Initial'), T.txt('Missed'), T.txt('Won'),
                            T.txt('Canceled By User'), T.txt('Canceled'), T.txt('Refund'),
                            T.txt('Canceled By Admin'), T.txt("Tie Refund")
                            ];
                            if ($scope.pager.totalItems == 0) {
                                $scope.sLoadingInfo = T.txt("No Data");
                            } else {
                                $scope.bLoading = false;
                            }
                        },
                        function (data) {
                            $scope.sLoadingInfo = "Error: " + data.error_code;
                        }
                    );
                };
            };

            $scope.cancelOrder = function (index) {

                if (confirm($scope.oi18n.confirmDelete)) {
                    $scope.dRequest = {};
                    $scope.dRequest.orderId = $scope.oData[index].orderNo;

                    var cancelFunc = function () {
                        $scope.oSearch = ConstService.getSearchBar('orderList');
                        $scope.bLoading = true;
                        $scope.sLoadingInfo = T.txt('Loading') + "...";

                        OrderService.rollBackOrder($scope.dRequest).then(
                            function (data) {

                                $scope.transStatus = [T.txt('Initial'), T.txt('Missed'), T.txt('Won'),
                                T.txt('Canceled By User'), T.txt('Canceled'), T.txt('Refund'),
                                T.txt('Canceled By Admin'), T.txt("Tie Refund")];
                                $scope.bLoading = false;
                                alert(data.description);

                                OrderService.getList($rootScope.dRequest).then(
                                    function (data) {
                                        $scope.oData = data.pageContent;
                                        $scope.pager.totalItems = data.itemTotal;
                                        console.log(data);
                                        $scope.transStatus = [
                                            T.txt('Initial'), T.txt('Missed'), T.txt('Won'),
                                            T.txt('Canceled By User'), T.txt('Canceled'), T.txt('Refund'),
                                            T.txt('Canceled By Admin')];


                                        $scope.bLoading = false;
                                    }
                                );
                            }
                        );

                    };
                    cancelFunc();
                }

            };


        }
    ])
    // Modals Instances (confirm button, ok button, cancel button) of modals events
    .controller('ModalInstanceCtrl', ['$rootScope', '$scope', 'OrderService', 'AccountDetailService', 'ReportService',
        'UserService', 'ModalInstanceExport', '$modalInstance', '$window', 'T', 'SearchBarService',
        function ($rootScope, $scope, OrderService, AccountDetailService, ReportService,
            UserService, ModalInstanceExport, $modalInstance, $window, T, SearchBarService) {
            $scope.oi18n = { "txt": T.T };

            /* START: select option gameType */
            const gameTypeLabel = {
                0: "All Games",
                5: "ticash",
                6: "dice",
                7: "coin",
            };

            $scope.gameTypeArray = [
                { type: "", code: "", label: T.txt('All Games') },
            ];

            SearchBarService.getLotteryCodeList().then(function (data) {
                var lotteryCodeData = data.data;
                // console.info("lotteryCodeLists", lotteryCodeData.data); 

                for (var key in lotteryCodeData.data) {
                    $scope.gameTypeArray.push({
                        type: lotteryCodeData.data[key].code,
                        code: lotteryCodeData.data[key].name,
                        label: gameTypeLabel[lotteryCodeData.data[key].code]
                    });
                }
            });

            $scope.getGameType = function (e) {

                // console.info("gameType", e); 

                // return T.txt(gameType[e]); 

                if (e === 'newBomb') {
                    return T.txt('ticash');
                } else if (e === 'dice') {
                    return T.txt('dice');
                } else {
                    return T.txt('All Games');
                }

            }
            /* END: select option gameType */



            // Confirm Export Button defending on the encodedParams validation
            $scope.confirm = function () {
                //$rootScope.encodedParams.wid = AuthService.getWid(); // for export wid

                console.group("Controller: start Export");
                console.groupCollapsed("$rootScope.dRequest");
                console.info($rootScope.dRequest);
                console.groupEnd();

                ModalInstanceExport.addInput();     // select the appropriate input for the api

                console.info($rootScope.encodedParams);
                //if($rootScope.encodedParamsStatus || ($rootScope.dRequest.hasOwnProperty("orderStatus"))) {
                //if($rootScope.encodedParamsStatus) {
                if ($rootScope.dRequest.hasOwnProperty("orderStatus")) {
                    $rootScope.encodedParams.orderStatus = $scope.dRequest.orderStatus;
                }

                $rootScope.encodedParams.pageSize = 35000;
                // console.groupCollapsed("encodedParams");
                console.info($rootScope.encodedParams);
                console.groupEnd();
                if ($rootScope.QueryType === "order") {  // Export Order
                    OrderService.exportOrder($rootScope.encodedParams).then(function (data) {
                        console.groupCollapsed("Controller: Responsed Data order queryType");
                        console.info(data); // this is the csv string format
                        console.groupEnd();
                        ModalInstanceExport.doExport("order", data);
                    });
                } else if ($rootScope.QueryType === "account") { // Export Account Details
                    AccountDetailService.exportAccountDetails($rootScope.encodedParams).then(function (data) {
                        console.groupCollapsed("Controller: Responsed Data account queryType");
                        console.info(data); // this is the csv string format
                        console.groupEnd();
                        ModalInstanceExport.doExport("account", data);
                    });
                } else if ($rootScope.QueryType === "userdaily") { // 用户日结 导出数据 CSV Export User Daily

                    // $rootScope.encodedParams.do = "findUserDailyReportList";


                    //*** export csv base on different gameType ***
                    if ($scope.oSearch.hasOwnProperty("gameTypeArray")) {
                        $rootScope.encodedParams.gameType = $scope.oSearch.gameTypeArray.type;
                    }
                    // $rootScope.encodedParams.gameType = 5;

                    ReportService.exportUserDaily($rootScope.encodedParams).then(function (data) {
                        console.info("Controller: Responsed Data userdaily queryType");
                        console.info("data from server:", data);
                        console.groupEnd();
                        ModalInstanceExport.doExport("userdaily", data);
                    });
                } else if ($rootScope.QueryType === "userlists") {  // Export User Lists
                    $rootScope.encodedParams.do = "getAdminList";
                    UserService.exportUserLists($rootScope.encodedParams).then(function (data) {
                        console.groupCollapsed("Controller: Responsed Data User Lists queryType");
                        console.info($rootScope.encodedParams);
                        console.groupEnd();
                        ModalInstanceExport.doExport("userlists", data);
                    });
                }
                $rootScope.encodedParams = {};
                //} else {
                //        console.log("parameters is required to export");
                //        $rootScope.ExportDataMessage = "cannot identify order status!";
                //        $rootScope.open('modal-message modal-danger','DangerModal.html');
                //}
                console.groupEnd("end Export");

                $modalInstance.close();
            };

            $scope.ok = function () {
                $modalInstance.close();
            };

            $scope.cancel = function () {
                $modalInstance.close();
            };
        }])
    // unused controller
    .controller('SideCtrl', [
        '$rootScope', '$scope', 'ReportService',
        function ($rootScope, $scope, ReportService) {

        }
    ])
    .controller('OrderDetailCtrl', [
        '$rootScope', '$scope', 'OrderService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', '$stateParams',
        function ($rootScope, $scope, OrderService, dateService, AuthService, $state, ConstService, $location, $stateParams) {
            $scope.pager = { totalItems: 10, maxSize: 10 };
            $rootScope.dRequest = { "page": 1, "pageSize": 20 };
            if ($stateParams.orderId && $stateParams.orderId.length > 0) {
                $scope.orderId = $stateParams.orderId;
                $scope.bDetail = true;
                $rootScope.dRequest.orderId = $stateParams.orderId;
            }
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.pager = { totalItems: 10, maxSize: 10 };
            $scope.init = function () {
                $scope.dRequest.orderBy = 0;
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    OrderService.getDetail($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.data = data;
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
            };
        }

    ]);





