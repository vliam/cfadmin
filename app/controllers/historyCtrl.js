app.controller('HistoryCtrl', [
    '$rootScope', '$scope', 'HistoryService', 'UpdateBonusNumberService', 'dateService', 'AuthService', '$state', 'ConstService', '$filter', '$stateParams', 'T', 'SearchBarService',
    function ($rootScope, $scope, HistoryService, UpdateBonusNumberService, dateService, AuthService, $state, ConstService, $filter, $stateParams, T, SearchBarService) {
        $scope.pager = { totalItems: 10, maxSize: 10, type: "" };
        $rootScope.dRequest = { "page": 1, "pageSize": 20 };
        /////////////////////////////////////
        $rootScope.User = AuthService.getCurrentUser();
        if ($scope.User) {
        } else {
            $state.go("login");
        }




        $scope.showExtraButtons = function () {
            $scope.isShowAllButton = true;
        };


        $scope.aClass = ["", "", "numeric", ""];
        $scope.pageChanged = function () {
            $scope.requestFunc();
        };
        //user report的时候调用
        $scope.initHistory = function () {
            $scope.oSearch = ConstService.getSearchBar('history');//TODO 加载search bar 配置

            /* START: select option gameType */
            // $rootScope.oSearch.gameTypeArrayLastRequest = {}; 
            const gameLotteryCode = {
                newBomb: { label: "ticash", code: "newBomb" },
                dice: { label: "dice", code: "dice" },
                coin: { label: "coin", code: "coin" },
                sm: { label: "spaceman", code: "sm" },
                mb: { label: "bobworker", code: "mb" },
                fruit: { label: "fruitcard", code: "fruit" }
            };


            $scope.gameTypeArray = [
                { type: "", code: "", label: T.txt('All Games') },
            ];

            SearchBarService.getLotteryCodeList().then(function (data) {
                var lotteryCodeData = data.data;
                for (var key in lotteryCodeData.data) {
                    $scope.gameTypeArray.push({
                        type: lotteryCodeData.data[key].code,
                        code: gameLotteryCode[lotteryCodeData.data[key].name].code,
                        label: gameLotteryCode[lotteryCodeData.data[key].name].label
                    })
                }
                $scope.oSearch.gameTypeArray = $scope.gameTypeArray[$rootScope.reportHistorySearchGameIndex];
                console.info("%coSearch.gameTypeArray", "color:#ffff00", $scope.oSearch.gameTypeArray);
                $scope.requestFunc();
            });

            $scope.getGameType = function (e) {
                return T.txt(gameLotteryCode[e].label);
            }
            /* END: select option gameType */


            $scope.oi18n = T.getI18n("HistoryCtrl");

            $scope.aTitle = [//TODO 报表标题
                [$scope.oi18n.time, ''],
                [$scope.oi18n.issue, ''],
                [$scope.oi18n.arena, ''],
                [$scope.oi18n.state, ''],
                [$scope.oi18n.results, ''],
                //[$scope.oi18n.modify, '']
            ];

            $scope.aStatus = [
                $scope.oi18n.initial,
                $scope.oi18n.inStock,
                $scope.oi18n.notEntered,
                $scope.oi18n.inTheAward,
                $scope.oi18n.beenAwarded
            ];

            $scope.statusColor = [
                {},
                {},
                {
                    "color": "red",
                    "font-weight": "bold"
                },
                {
                    "color": "blue",
                    "font-weight": "bold"
                },
                {}
            ];


            $scope.oNumber = { 'W': $scope.oi18n.W, 'M': $scope.oi18n.M, 'C': $scope.oi18n.C, 'D': $scope.oi18n.D };
            /*  $scope.transStatus = function (iStatus) {
                var aStatus = ['', 'status1 ', 'status2','status3'];
                console.log(iStatus);
                return aStatus[iStatus];
            };*/
            $scope.requestFunc = function () {//点击搜索
                $scope.bLoading = true;
                $scope.sLoadingInfo = T.txt('Loading') + "...";

                // $rootScope.dRequest.lotteryCode = "newBomb";

                /* 游戏种类*/
                if ($scope.oSearch.gameTypeArray) {
                    console.log("%cHistoryCtrl oSearch", "color:#00FFFF;", $scope.oSearch.gameTypeArray);

                    for (var key in $scope.gameTypeArray) {
                        if ($scope.gameTypeArray[key].type === $scope.oSearch.gameTypeArray.type) {
                            $rootScope.reportHistorySearchGameIndex = key;
                            break;
                        }
                    }
                    $rootScope.dRequest.lotteryCode = $scope.oSearch.gameTypeArray.code;
                }

                $rootScope.dRequest.status = 9;

                if ($stateParams.gameNumber && $stateParams.gameNumber.length > 0) {
                    $rootScope.dRequest.issue = $stateParams.gameNumber;
                }
                HistoryService.getHistory($rootScope.dRequest).then(
                    function (data) {
                        console.log(data);//这里是数据，数组每一条为一行
                        $scope.oData = data.pageContent;
                        //把数字的status转换为文字

                        $scope.pager.totalItems = data.itemTotal;
                        $scope.bLoading = false;
                    }
                );
            };
        };
        // $scope.bShowList =[];
        $scope.updateBonusNumber = function (i) {
            $scope.bShowList = [];
            $scope.bShowList[i] = true;
            // console.log($scope.bShowList);
            $scope.oSelect = { value: '' };
        };



        $scope.save = function (rowData, i) {

            if ($scope.oSelect.value == '') {
                $scope.oSelect.value = rowData.bonusNumber;
            }

            var updateBonusNumInfo = {
                'timestamp': dateService.stringCurrTime(),
                'issue': rowData.issue,
                'result': $scope.oSelect.value,
                'lotterycode': rowData.lotterycode
            };

            if ($scope.oSelect.value != rowData.bonusNumber) {
                if (confirm($scope.oi18n.confirm)) {
                    UpdateBonusNumberService.getBounusInfo(updateBonusNumInfo).then(
                        function (data) {

                            //TODO 绑定到前台
                            console.log(data.code);
                            if (data.code == '1') {
                                rowData.bonusNumber = $scope.oSelect.value;
                            } else {
                                alert(data.description);
                            }

                        }
                    );
                }

            }

            $scope.bShowList[i] = false;

        }
    }
]);
