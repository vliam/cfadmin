app
    .controller('UserCtrl', [
        '$rootScope', '$scope', 'UserService', 'dateService', 'AuthService', '$state', 'ConstService', '$location', '$stateParams', 'T',
        function ($rootScope, $scope, UserService, dateService, AuthService, $state, ConstService, $location, $stateParams, T) {
            $scope.pager = {totalItems: 10, maxSize: 10};
            $rootScope.dRequest = {"page": 2, "pageSize": 10};
            if( $stateParams.userName && $stateParams.userName.length >0){
                $rootScope.userName = $stateParams.userName;
                $scope.bDetail =true;
                $rootScope.dRequest.userName =$stateParams.userName;
            }
            if($stateParams.ip){
                $rootScope.dRequest.ip =$stateParams.ip;
                console.log( $rootScope.dRequest);
            }
            /////////////////////////////////////
            $rootScope.User = AuthService.getCurrentUser();
            if ($scope.User) {
            } else {
                $state.go("login");
            }
            $scope.goDetail = function (sUserName) {
                $location.path('/UserPage/' + sUserName);
            };
            $scope.mask = function (str) {
                if (!str)return "";
                var aStr = str.split("|");
                var sRes = aStr[0].substring(0, 3) + "****" + aStr[0].substring(7, 11);
                if (aStr.length > 1) {
                    sRes += "|" + aStr[1];
                }
                return sRes;
            };
            $scope.getUserStatus = function (iStatus) {
                return ConstService.getUserStatus(iStatus);
            };

            $scope.oi18n = T.getI18n("UserCtrl");

            $scope.aTitle = [
                [$scope.oi18n.Rtime, ''],
                [$scope.oi18n.user, ''],
                [$scope.oi18n.balance, 'numeric'],
                [$scope.oi18n.login, '']
                //[$scope.oi18n.status, '']
            ];
            //$scope.aStatus = [];
            $scope.aClass = ["", "", "numeric", ""];
            $scope.pageChanged = function () {
                $scope.requestFunc();
            };
            $scope.pager = {totalItems: 10, maxSize: 10};
            $scope.init = function () {
                $scope.dRequest.orderBy = 0;

                $scope.aStatus = [{i: 1, label: '活跃'}, {i: 2, label: '冻结'}];
                $scope.aType = [{i: 1, label: '普通用户'}, {i: 2, label: '测试用户'}];

                $scope.requestFunc = function () {
                    $scope.oSearch = ConstService.getSearchBar('userList');
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    UserService.getList($rootScope.dRequest).then(
                        function (data) {
                            console.log("uio",data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;

                            if ($scope.pager.totalItems == 0 ){
                                $scope.sLoadingInfo = T.txt("No Data");
                            }else{
                                $scope.bLoading = false;
                            }
                        },function(data){
                            alert("Error UserList");
                            $scope.bLoading = false;
                        }
                    );
                };
                $scope.requestFunc();
                
                /////////////////Lock Controller
                $scope.clickLock = function(username){ //pass parameter from view
                    var result = confirm('Are you sure?');//confirmation
                    if(result === true){
                    $scope.requestFunc = function () {//show the data again
                        $scope.oSearch = ConstService.getSearchBar('userList');
                        $scope.bLoading = true;
                        $scope.sLoadingInfo = T.txt('Loading') + "...";

                        UserService.lockUser({do:'frozenUser',wid:'t1',userName: username,type:2}).then(

                            function (data) {
                                $scope.oSearch = ConstService.getSearchBar('userList');
                                $scope.bLoading = true;
                                $scope.sLoadingInfo = T.txt('Loading') + "...";

                                UserService.getList($rootScope.dRequest).then(
                                    function (data) {
                                        $scope.oData = data.pageContent;
                                        $scope.pager.totalItems = data.itemTotal;

                                        if ($scope.pager.totalItems == 0 ){
                                            $scope.sLoadingInfo = T.txt("No Data");
                                        }else{
                                            $scope.bLoading = false;
                                        }
                                    },function(data){
                                        alert("Error UserList");
                                        $scope.bLoading = false;
                                    }
                                );

                            },function(data){//error message
                                alert("Error UserList");
                                $scope.bLoading = false;
                            }
                        );
                    };
                    $scope.requestFunc();
                    }
                }
                ////////////////End of Lock Controller
                
                /////////////////Unlock Controller
                $scope.clickUnlock = function(username){ //pass parameter from view
                    console.log('123');
                    var result = confirm('Are you sure?');//confirmation
                    if(result === true){
                    $scope.requestFunc = function () {//show the data again
                        $scope.oSearch = ConstService.getSearchBar('userList');
                        $scope.bLoading = true;
                        $scope.sLoadingInfo = T.txt('Loading') + "...";

                        UserService.lockUser({do:'frozenUser',wid:'t1',userName: username,type:1}).then(

                            function (data) {
                                $scope.oSearch = ConstService.getSearchBar('userList');
                                $scope.bLoading = true;
                                $scope.sLoadingInfo = T.txt('Loading') + "...";

                                UserService.getList($rootScope.dRequest).then(
                                    function (data) {
                                        $scope.oData = data.pageContent;
                                        $scope.pager.totalItems = data.itemTotal;

                                        if ($scope.pager.totalItems == 0 ){
                                            $scope.sLoadingInfo = T.txt("No Data");
                                        }else{
                                            $scope.bLoading = false;
                                        }
                                    },function(data){
                                        alert("Error UserList");
                                        $scope.bLoading = false;
                                    }
                                );

                            },function(data){//error message
                                alert("Error UserList");
                                $scope.bLoading = false;
                            }
                        );
                    };
                    $scope.requestFunc();
                    }
                }
                ////////////////End of UnLock Controller
                
                
                
            };
            $scope.showExportButton = function() {
                $scope.isExportButton = true;
                $rootScope.QueryType = "userlists";
                $scope.isShowAllButton = true;
            };
            //user login log, unused 07052017
            $scope.initIP = function () {//ip页面
                $scope.aTitle = [
                    ['时间', ''],
                    ['用户名', ''],
                    ['IP', ''],
                    ['定位', '']
                ];
                $scope.oSearch = ConstService.getSearchBar('ip');
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    $scope.oSearch = ConstService.getSearchBar('loginLog');
                    UserService.getLoginLog($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            if ($scope.pager.totalItems == 0 ){
                                $scope.sLoadingInfo = T.txt("No Data");
                            }else{
                                $scope.bLoading = false;
                            }
                        }
                    );
                };
                $scope.requestFunc();
            };
            //illegal requests , unused 07052017
            $scope.initIllegal = function () {//非法用户
                $scope.aTitle = [
                    ['时间', ''],
                    ['用户名', ''],
                    ['信息', '']
                ];
                $scope.requestFunc = function () {
                    $scope.bLoading = true;
                    $scope.sLoadingInfo = T.txt('Loading') + "...";

                    $scope.oSearch = ConstService.getSearchBar('illegal');
                    UserService.getIllegalList($rootScope.dRequest).then(
                        function (data) {
                            console.log(data);
                            $scope.oData = data.pageContent;
                            $scope.pager.totalItems = data.itemTotal;
                            if ($scope.pager.totalItems == 0 ){
                                $scope.sLoadingInfo = T.txt("No Data");
                            }else{
                                $scope.bLoading = false;
                            }
                        }
                    );
                };
                $scope.requestFunc();
            };
        }
    ]);