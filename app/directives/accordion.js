angular.module('app')
.directive('simpleAccordion', function () {
    return {
        // attribute
        restrict: 'A',
        scope: {
            // default: '400ms'
            // options: 'milliseconds', 'slow', or 'fast'
            toggleSpeed: '@toggleSpeed',
            slideUpSpeed: '@slideUpSpeed',
            // default: 'swing'
            // options: 'swing', 'linear'
            toggleEasing: '@toggleEasing',
            slideUpEasing: '@slideUpEasing'
        },
        link: function (scope, element, attrs) {
            
//            element.find('.accordion .toggle').click(function(){
            element.on('click','.accordion .toggle',function(){ //delegate
                var elem = $(this);
                if (elem.next().hasClass('showed')) {
                    elem.next().removeClass('showed');
                    elem.next().next().slideDown(350);
                } else{
                    elem.parent().parent().find('ul.inner').removeClass('showed');
                    elem.parent().parent().find('ul.inner').slideDown(350);
                    elem.next().toggleClass('showed');
                    elem.next().next().slideToggle(350);
                }
            });
        }
    };
});